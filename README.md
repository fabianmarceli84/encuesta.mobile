# Encuesta Project

## Getting Started

🔨 Worked with:

```
Flutter 3.3.10 • channel stable • https://github.com/flutter/flutter.git
Framework • revision 135454af32 (8 weeks ago) • 2022-12-15 07:36:55 -0800
Engine • revision 3316dd8728
Tools • Dart 2.18.6 • DevTools 2.15.0
```

## How to Use

**❖ Step 1:**

✔️ Download or clone this repo by using the link below:

```
git clone https://gitlab.com/iot.abx/encuesta.mobile.git
```

**❖ Step 2:**

✔️ Go to project flutter:

```
cd app
```

**❖ Step 3:**

✔️ Go to project root and execute the following command in console to get the required dependencies:

```
flutter pub get
```

**❖ Step 4:**

✔️ The project makes use of generated code for:

- Drift (Moor): Database 🧠
- Mobx: State Management ⚙️

```
flutter pub run build_runner build --delete-conflicting-outputs
```

- ⚠️ Use it when making a strong modification
