import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFormFieldCustom extends StatelessWidget {
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final String? labelText;
  final String? errorText;
  final String? initialValue;
  final String? regExp;
  final bool isObscure;
  final bool enabled;
  final TextCapitalization? textCapitalization;
  final TextInputType? inputType;
  final TextEditingController? textController;
  final Color? fillColor;
  final Color errorColor;
  final Color lineColor;
  final Color labelColor;
  final TextAlign textAlign;
  final FocusNode? focusNode;
  final ValueChanged? onFieldSubmitted;
  final ValueChanged? onChanged;
  final bool autoFocus;
  final TextInputAction? inputAction;
  final int? maxLength;
  final int? maxLines;
  final int? minLines;

  const TextFormFieldCustom({
    Key? key,
    this.prefixIcon,
    this.suffixIcon,
    this.textController,
    this.textCapitalization,
    this.errorText,
    this.inputType,
    this.labelText,
    this.initialValue,
    this.regExp,
    this.isObscure = false,
    this.focusNode,
    this.onFieldSubmitted,
    this.onChanged,
    this.autoFocus = false,
    this.inputAction,
    this.maxLength = 50,
    this.enabled = true,
    this.textAlign = TextAlign.start,
    this.maxLines = 1,
    this.minLines,
    this.fillColor,
    this.errorColor = AppColor.accent,
    this.lineColor = AppColor.selectionColor,
    this.labelColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autocorrect: false,
      controller: textController,
      textCapitalization: textCapitalization ?? TextCapitalization.none,
      cursorColor: Theme.of(context).textSelectionTheme.cursorColor,
      maxLength: maxLength,
      minLines: minLines,
      maxLines: maxLines,
      autofocus: autoFocus,
      obscureText: isObscure,
      textInputAction: inputAction,
      onChanged: onChanged,
      onFieldSubmitted: onFieldSubmitted,
      focusNode: focusNode,
      keyboardType: inputType,
      enabled: enabled,
      initialValue: initialValue,
      inputFormatters: regExp == null
          ? null
          : <TextInputFormatter>[
              FilteringTextInputFormatter.allow(RegExp(regExp!)),
            ],
      style: const TextStyle(
        // color: labelColor,
        fontSize: 18,
      ),
      decoration: InputDecoration(
        counterText: '',
        errorText: errorText,
        suffixIcon: suffixIcon,
        prefixIcon: prefixIcon,
        filled: true,
        fillColor: fillColor ?? AppColor.backgroundTextFormField,
        contentPadding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 15,
        ),
        errorStyle: TextStyle(
          color: errorColor,
          fontSize: 14,
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            Radius.circular(10.0),
          ),
          borderSide: BorderSide(
            color: errorColor,
            width: 2,
          ),
        ),
        border: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            Radius.circular(10.0),
          ),
          borderSide: BorderSide(
            color: lineColor,
            width: 2,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            Radius.circular(10.0),
          ),
          borderSide: BorderSide(
            color: lineColor,
            width: 2,
          ),
        ),
        labelText: labelText,
        // labelStyle: TextStyle(
        //   color: labelColor,
        //   fontSize: 14,
        // ),
        enabledBorder: OutlineInputBorder(
          borderRadius: const BorderRadius.all(
            Radius.circular(10.0),
          ),
          borderSide: BorderSide(
            color: lineColor,
            width: 2,
          ),
        ),
      ),
    );
  }
}
