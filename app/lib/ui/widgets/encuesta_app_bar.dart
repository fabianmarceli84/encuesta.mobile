import 'package:animate_do/animate_do.dart';
import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:app_mobile_encuesta/constants/ui.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class EncuestaAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  final Size preferredSize;
  final Color? color;
  final Color? colorLogo;
  final bool onlyLogo;
  final bool animate;
  final Widget? child;
  final Widget? leading;
  final double? leadingWidth;
  final List<Widget>? actions;

  EncuestaAppBar({
    Key? key,
    this.color,
    this.colorLogo,
    this.onlyLogo = false,
    this.animate = false,
    this.child,
    this.leading,
    this.leadingWidth,
    this.actions,
  })  : preferredSize = const Size.fromHeight(Ui.kToolbarHeight),
        super(key: key) {
    if (onlyLogo == true && child != null) {
      throw FlutterError('if you only use logo, \n'
          'It is not necessary to use child');
    }
  }

  @override
  Widget build(BuildContext context) {
    return SlideInDown(
      from: animate ? 100.0 : 0.0,
      animate: animate,
      child: AppBar(
        automaticallyImplyLeading: onlyLogo ? false : true,
        leading: onlyLogo ? const SizedBox.shrink() : leading,
        leadingWidth: onlyLogo ? 0.0 : leadingWidth ?? Ui.kToolbarHeight,
        titleSpacing: 0.0,
        bottomOpacity: 1.0,
        centerTitle: onlyLogo ? true : false,
        actions: onlyLogo ? null : actions,
        foregroundColor: Colors.white,
        backgroundColor: color ?? AppColor.primaryColor,
        shadowColor: Colors.transparent,
        elevation: 0.0,
        title: onlyLogo
            ? Image.asset(
                'assets/images/encuesta_text.png',
                semanticLabel: 'Encuesta',
                color: colorLogo ?? Colors.white,
                height: 35.0,
              )
            : child,
      ),
    );
  }
}
