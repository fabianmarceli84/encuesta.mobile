import 'package:flutter/material.dart';

class EncuestaBanner extends StatelessWidget {
  final String assetPath;
  final BoxFit? fit;
  final AlignmentGeometry? alignment;
  const EncuestaBanner(
    this.assetPath, {
    Key? key,
    this.fit,
    this.alignment,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Image(
      image: AssetImage(assetPath),
      fit: fit ?? BoxFit.fitWidth,
      alignment: alignment ?? FractionalOffset.bottomLeft,
    );
  }
}
