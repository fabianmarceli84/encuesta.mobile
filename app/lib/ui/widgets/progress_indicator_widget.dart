import 'package:flutter/material.dart';

class CustomProgressIndicatorWidget extends StatelessWidget {
  final Color? shadowColor;
  final Color? backgroundColor;
  final Color? circleColor;
  final bool showShadow;

  const CustomProgressIndicatorWidget({
    Key? key,
    this.shadowColor,
    this.backgroundColor,
    this.circleColor,
    this.showShadow = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Align(
        alignment: Alignment.center,
        child: Container(
          height: 100,
          constraints: const BoxConstraints.expand(),
          decoration: showShadow
              ? BoxDecoration(
                  color: shadowColor ?? Colors.grey.withAlpha(80),
                )
              : null,
          child: FittedBox(
            fit: BoxFit.none,
            child: SizedBox(
              height: 100,
              width: 100,
              child: Card(
                elevation: showShadow ? 1.0 : 0,
                shadowColor: showShadow ? null : Colors.transparent,
                color:
                    backgroundColor ?? Theme.of(context).colorScheme.background,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: CircularProgressIndicator(
                    color: circleColor ?? Theme.of(context).colorScheme.primary,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
