import 'package:flutter/material.dart';

class ShowHideEye extends StatelessWidget {
  final bool obscure;
  final Function(bool obscure)? onPressed;
  final Color? foregroundColor;
  final double? size;

  const ShowHideEye({
    Key? key,
    this.obscure = true,
    this.onPressed,
    this.foregroundColor = Colors.white,
    this.size = 24,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      hoverColor: Colors.transparent,
      icon: Icon(
        obscure ? Icons.visibility_rounded : Icons.visibility_off_rounded,
        size: size,
        color: foregroundColor,
      ),
      onPressed: () {
        if (onPressed != null) onPressed!(!obscure);
      },
    );
  }
}
