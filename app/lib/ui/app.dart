import 'package:app_mobile_encuesta/constants/app_theme.dart';
import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:app_mobile_encuesta/constants/strings.dart';
import 'package:app_mobile_encuesta/core/stores/app/app_store.dart';
import 'package:app_mobile_encuesta/core/stores/setting/setting_store.dart';
import 'package:app_mobile_encuesta/core/stores/theme/theme_store.dart';
import 'package:app_mobile_encuesta/di/components/service_locator.dart';
import 'package:app_mobile_encuesta/ui/pages/splash/splash.dart';
import 'package:app_mobile_encuesta/utils/routes/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  // This widget is the root of your application.
  final ThemeStore _themeStore = getIt<ThemeStore>();
  final AppStore _appStore = getIt<AppStore>();
  final SettingStore _settingStore = getIt<SettingStore>();

  Future<void> _setSystemUIStyle(
    bool darkMode,
    bool fullScreenMode,
    int primaryColor,
  ) async {
    AppColor.primaryColor = Color(primaryColor);
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        systemNavigationBarColor: darkMode
            ? AppColor.backgroundItemDark
            : AppColor.backgroundItemLight,
        systemNavigationBarDividerColor: darkMode
            ? AppColor.backgroundItemDark
            : AppColor.backgroundItemLight,
        systemNavigationBarIconBrightness:
            darkMode ? Brightness.light : Brightness.dark,
      ),
    );

    final SystemUiMode mode;
    final List<SystemUiOverlay> overlays = [];

    if (fullScreenMode) {
      mode = SystemUiMode.immersiveSticky;
    } else {
      mode = SystemUiMode.edgeToEdge;
    }

    await SystemChrome.setEnabledSystemUIMode(
      mode,
      overlays: overlays,
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<ThemeStore>(create: (_) => _themeStore),
        Provider<AppStore>(create: (_) => _appStore),
        Provider<SettingStore>(create: (_) => _settingStore),
      ],
      child: Observer(
        name: 'global-observer',
        builder: (context) {
          final darkMode = _themeStore.darkMode;
          final fullScreenMode = _themeStore.fullScreenMode;
          final primaryColor = _themeStore.primaryColor;

          _setSystemUIStyle(
            darkMode,
            fullScreenMode,
            primaryColor,
          );

          return GestureDetector(
            onTap: () {
              FocusManager.instance.primaryFocus?.unfocus();
            },
            child: MaterialApp(
              debugShowCheckedModeBanner: false,
              title: AppInfo.appName,
              theme: _themeStore.darkMode
                  ? AppTheme.getThemeDataDark(primaryColor)
                  : AppTheme.getThemeData(primaryColor),
              routes: Routes.routes,
              localizationsDelegates: const [
                GlobalMaterialLocalizations.delegate,
                GlobalCupertinoLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
              ],
              supportedLocales: const [
                Locale('en', 'US'), // Inglés
                Locale('es', 'PE'), // Español
              ],
              locale: const Locale('es', 'PE'),
              home: const SplashScreen(),
            ),
          );
        },
      ),
    );
  }
}
