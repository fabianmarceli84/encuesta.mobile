import 'dart:async';
import 'package:animate_do/animate_do.dart';
import 'package:app_mobile_encuesta/core/stores/app/app_store.dart';
import 'package:app_mobile_encuesta/core/stores/theme/theme_store.dart';
import 'package:app_mobile_encuesta/di/components/service_locator.dart';
import 'package:app_mobile_encuesta/ui/pages/login/login.dart';
import 'package:app_mobile_encuesta/ui/pages/login_save/login_save.dart';
import 'package:app_mobile_encuesta/utils/transitions/page_transitions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final ThemeStore _themeStore = getIt<ThemeStore>();
  final AppStore _appStore = getIt<AppStore>();

  _startTimer() {
    var duration = const Duration(milliseconds: 1500);
    return Timer(duration, _navigate);
  }

  _navigate() async {
    Widget nextPage;

    if (_appStore.isLoggedIn) {
      nextPage = const LoginSaveScreen();
    } else {
      nextPage = const LoginScreen();
    }

    Navigator.pushAndRemoveUntil(
      context,
      FadeRoute(page: nextPage),
      (Route<dynamic> route) => false,
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      _startTimer();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: _themeStore.darkMode
          ? Colors.black
          : Theme.of(context).colorScheme.background,
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ZoomIn(
            child: FadeOut(
              delay: const Duration(milliseconds: 2000),
              animate: true,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Flash(
                    duration: const Duration(milliseconds: 1000),
                    child: SvgPicture.asset(
                      'assets/icons/logo.svg',
                      semanticsLabel: 'Logo',
                      height: 150,
                      color: _themeStore.darkMode ? Colors.white : Colors.black,
                    ),
                  ),
                  ElasticIn(
                    delay: const Duration(milliseconds: 1000),
                    child: Image.asset(
                      'assets/images/encuesta_text.png',
                      semanticLabel: 'Encuesta',
                      color: _themeStore.darkMode ? Colors.white : Colors.black,
                      width: 200,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
