import 'package:animate_do/animate_do.dart';
import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:app_mobile_encuesta/core/stores/app/app_store.dart';
import 'package:app_mobile_encuesta/core/stores/login/login_store.dart';
import 'package:app_mobile_encuesta/core/stores/theme/theme_store.dart';
import 'package:app_mobile_encuesta/di/components/service_locator.dart';
import 'package:app_mobile_encuesta/ui/pages/login_auth/login_unlock.dart';
import 'package:app_mobile_encuesta/ui/pages/main/main_drawer.dart';
import 'package:app_mobile_encuesta/ui/widgets/encuesta_app_bar.dart';
import 'package:app_mobile_encuesta/ui/widgets/progress_indicator_widget.dart';
import 'package:app_mobile_encuesta/ui/widgets/show_hide_eye.dart';
import 'package:app_mobile_encuesta/ui/widgets/text_form_field_custom.dart';
import 'package:app_mobile_encuesta/utils/device/device_utils.dart';
import 'package:app_mobile_encuesta/utils/message/message.dart';
import 'package:app_mobile_encuesta/utils/style/button_style.dart';
import 'package:app_mobile_encuesta/utils/transitions/page_transitions.dart';
import 'package:app_mobile_encuesta/utils/validators/validators.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:lottie/lottie.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final ThemeStore _themeStore = getIt<ThemeStore>();
  final AppStore _appStore = getIt<AppStore>();
  final LoginStore _loginStore = getIt<LoginStore>();

  final TextEditingController _usernameTextController = TextEditingController();
  final TextEditingController _passwordTextController = TextEditingController();

  bool isLoggedIn = false;

  Future<void> _onPressedBuildForm() async {
    if (!_loginStore.canLogin) {
      Message.showErrorMessage(
        context,
        'Complete los campos vacíos',
      );
      return;
    }
    DeviceUtils.hideKeyboard(context);
    _loginStore.login();
  }

  void _onPressedNavigate() {
    isLoggedIn = true;
    Future.delayed(const Duration(milliseconds: 0), () async {
      await _appStore.reload();

      // ignore: use_build_context_synchronously
      Navigator.pushAndRemoveUntil(
        context,
        UpperRoute(
          page: const LoginUnlockScreen(nextPage: MainDrawerScreen()),
        ),
        (Route<dynamic> route) => false,
      );
    });
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {});
  }

  @override
  void dispose() {
    _usernameTextController.dispose();
    _passwordTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: Scaffold(
        primary: true,
        resizeToAvoidBottomInset: false,
        appBar: EncuestaAppBar(
          onlyLogo: true,
          color: _themeStore.darkMode ? Colors.black : AppColor.primaryColor,
        ),
        body: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return NotificationListener<OverscrollIndicatorNotification>(
              onNotification: (overscroll) {
                overscroll.disallowIndicator();
                return true;
              },
              child: SingleChildScrollView(
                physics: const AlwaysScrollableScrollPhysics(),
                child: Container(
                  height: constraints.maxHeight,
                  width: constraints.maxWidth,
                  color: _themeStore.darkMode
                      ? AppColor.backgroundDark
                      : AppColor.backgroundLight,
                  child: Stack(
                    alignment: AlignmentDirectional.topCenter,
                    children: [
                      _BuildWallPaperBackgroud(
                        constraints: constraints,
                      ),
                      _BuildForm(
                        constraints: constraints,
                        usernameTextController: _usernameTextController,
                        passwordTextController: _passwordTextController,
                        loginStore: _loginStore,
                        themeStore: _themeStore,
                        onPressed: _onPressedBuildForm,
                      ),
                      _BuildVersionText(
                        appStore: _appStore,
                      ),
                      _BuildViewState(
                        loginStore: _loginStore,
                        navigate: _Navigate(
                          isLoggedIn: isLoggedIn,
                          onPressed: _onPressedNavigate,
                        ),
                      ),
                      _BuildLoading(
                        loginStore: _loginStore,
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class _BuildWallPaperBackgroud extends StatelessWidget {
  final BoxConstraints constraints;

  const _BuildWallPaperBackgroud({
    Key? key,
    required this.constraints,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: constraints.maxHeight * 0.190,
      width: constraints.maxWidth,
      child: Lottie.asset(
        'assets/lottie/login.json',
        repeat: true,
        animate: true,
      ),
    );
  }
}

class _BuildForm extends StatelessWidget {
  final BoxConstraints constraints;
  final TextEditingController usernameTextController;
  final TextEditingController passwordTextController;
  final LoginStore loginStore;
  final ThemeStore themeStore;
  final VoidCallback onPressed;

  const _BuildForm({
    Key? key,
    required this.constraints,
    required this.usernameTextController,
    required this.passwordTextController,
    required this.loginStore,
    required this.themeStore,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverFillRemaining(
          hasScrollBody: false,
          child: Column(
            children: [
              SizedBox(height: constraints.maxHeight * 0.190),
              FadeIn(
                child: SizedBox(
                  width: constraints.maxWidth,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      children: [
                        const SizedBox(height: 20),
                        Observer(
                          builder: (context) {
                            return TextFormFieldCustom(
                              textController: usernameTextController,
                              labelText: 'Usuario',
                              regExp: Validators.textCleanPattern,
                              maxLength: 30,
                              errorText: loginStore.formErrorStore.username,
                              onChanged: (value) {
                                loginStore
                                    .setUsername(usernameTextController.text);
                              },
                            );
                          },
                        ),
                        const SizedBox(height: 15),
                        Observer(
                          builder: (context) {
                            return TextFormFieldCustom(
                              textController: passwordTextController,
                              labelText: 'Clave',
                              regExp: Validators.textCleanPattern,
                              suffixIcon: ShowHideEye(
                                foregroundColor: themeStore.darkMode
                                    ? Colors.white
                                    : Colors.black,
                                obscure: loginStore.obscure,
                                onPressed: (obscure) {
                                  loginStore.setObscure(obscure);
                                },
                              ),
                              isObscure: loginStore.obscure,
                              maxLength: 30,
                              errorText: loginStore.formErrorStore.password,
                              onChanged: (value) {
                                loginStore
                                    .setPassword(passwordTextController.text);
                              },
                            );
                          },
                        ),
                        const SizedBox(height: 130),
                        ElevatedButton(
                          style: EncuestaButtonStyle.primary,
                          onPressed: onPressed,
                          child: const Text('Ingresar'),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class _BuildVersionText extends StatelessWidget {
  final AppStore appStore;

  const _BuildVersionText({
    Key? key,
    required this.appStore,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (context) {
        return Positioned.fill(
          child: Align(
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "Versión: ${appStore.packageInfo.version}",
                style: Theme.of(context).textTheme.caption,
              ),
            ),
          ),
        );
      },
    );
  }
}

class _BuildViewState extends StatelessWidget {
  final LoginStore loginStore;
  final Widget navigate;

  const _BuildViewState({
    Key? key,
    required this.loginStore,
    required this.navigate,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (context) {
        return loginStore.success
            ? navigate
            : Message.showErrorMessage(
                context,
                loginStore.errorStore.errorMessage,
              );
      },
    );
  }
}

class _BuildLoading extends StatelessWidget {
  final LoginStore loginStore;

  const _BuildLoading({
    Key? key,
    required this.loginStore,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (context) {
        return Visibility(
          visible: loginStore.loading,
          child: const CustomProgressIndicatorWidget(),
        );
      },
    );
  }
}

// General Methods:-----------------------------------------------------------
class _Navigate extends StatelessWidget {
  final bool isLoggedIn;
  final VoidCallback onPressed;

  const _Navigate({
    Key? key,
    required this.isLoggedIn,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (isLoggedIn) return const SizedBox.shrink();
    onPressed();
    return const SizedBox.shrink();
  }
}
