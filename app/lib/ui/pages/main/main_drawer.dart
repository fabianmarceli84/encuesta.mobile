import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:app_mobile_encuesta/core/stores/theme/theme_store.dart';
import 'package:app_mobile_encuesta/di/components/service_locator.dart';
import 'package:app_mobile_encuesta/ui/pages/login/login.dart';
import 'package:app_mobile_encuesta/ui/pages/logout/logout.dart';
import 'package:app_mobile_encuesta/ui/pages/main/main.dart';
import 'package:app_mobile_encuesta/ui/pages/security/security.dart';
import 'package:app_mobile_encuesta/ui/pages/setting/setting.dart';
import 'package:app_mobile_encuesta/ui/widgets/kf_drawer.dart';
import 'package:app_mobile_encuesta/utils/transitions/page_transitions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainDrawerScreen extends StatefulWidget {
  const MainDrawerScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MainDrawerScreenState();
}

class _MainDrawerScreenState extends State<MainDrawerScreen> {
  final ThemeStore _themeStore = getIt<ThemeStore>();

  late KFDrawerController _drawerController;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {});
    _drawerController = KFDrawerController(
      initialPage: MainScreen(),
      items: [
        KFDrawerItem.initWithPage(
          text: const Text('Configuración',
              style: TextStyle(color: Colors.white)),
          icon: const Icon(CupertinoIcons.gear, color: Colors.white),
          onPressed: () => _navigateMenu(const SettingScreen()),
        ),
        KFDrawerItem.initWithPage(
          text: const Text(
            'Seguridad y privacidad',
            style: TextStyle(color: Colors.white),
          ),
          icon: const Icon(CupertinoIcons.lock_shield, color: Colors.white),
          onPressed: () => _navigateMenu(const SecurityScreen()),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      primary: true,
      extendBodyBehindAppBar: true,
      resizeToAvoidBottomInset: false,
      body: KFDrawer(
        controller: _drawerController,
        scrollable: false,
        header: Align(
          alignment: Alignment.centerRight,
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            width: MediaQuery.of(context).size.width * 0.8,
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Image.asset(
                'assets/images/encuesta_text.png',
                semanticLabel: 'Encuesta',
                height: 35.0,
              ),
            ),
          ),
        ),
        footer: KFDrawerItem(
          text: const Text(
            'Cerrar sesión',
            style: TextStyle(color: Colors.white),
          ),
          icon: const Icon(
            CupertinoIcons.arrow_left_square,
            color: Colors.white,
          ),
          onPressed: _navigateLogout,
        ),
      ),
      backgroundColor: _themeStore.darkMode
          ? AppColor.backgroundDark
          : AppColor.backgroundLight,
    );
  }

  _navigateMenu(Widget nextPage) {
    Future.delayed(const Duration(milliseconds: 300)).then((_) {
      if (_drawerController.close != null) {
        _drawerController.close!();
      }
    });

    Navigator.push(
      context,
      UpperRoute(
        page: nextPage,
      ),
    );
  }

  _navigateLogout() async {
    Navigator.pushAndRemoveUntil(
        context,
        RightRoute(
          page: const LogoutScreen(
            nextPage: LoginScreen(),
          ),
        ),
        (Route<dynamic> route) => false);
  }
}
