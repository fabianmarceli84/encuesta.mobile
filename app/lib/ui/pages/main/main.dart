// ignore_for_file: must_be_immutable, use_build_context_synchronously

import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:app_mobile_encuesta/constants/ui.dart';
import 'package:app_mobile_encuesta/core/stores/app/app_store.dart';
import 'package:app_mobile_encuesta/core/stores/biometric/biometric_store.dart';
import 'package:app_mobile_encuesta/core/stores/setting/setting_store.dart';
import 'package:app_mobile_encuesta/di/components/service_locator.dart';
import 'package:app_mobile_encuesta/ui/pages/home/home_admin.dart';
import 'package:app_mobile_encuesta/ui/pages/home/home_voter.dart';
import 'package:app_mobile_encuesta/ui/pages/security/access/biometric/fingerprint_setting.dart';
import 'package:app_mobile_encuesta/ui/widgets/encuesta_app_bar.dart';
import 'package:app_mobile_encuesta/ui/widgets/kf_drawer.dart';
import 'package:app_mobile_encuesta/utils/extension/extensions.dart';
import 'package:app_mobile_encuesta/utils/transitions/page_transitions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:marquee/marquee.dart';

class MainScreen extends KFDrawerContent {
  MainScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen>
    with SingleTickerProviderStateMixin {
  final AppStore _appStore = getIt<AppStore>();
  final SettingStore _settingStore = getIt<SettingStore>();
  final BiometricStore _biometricStore = getIt<BiometricStore>();

  final PageController _mainPageViewController = PageController();
  late TabController _mainTabBarController;

  void _onItemTapped(int index) {
    // _consumerViewStore.jumpToPage(index);
    _mainPageViewController.jumpToPage(index);
  }

  _checkBiometricAuth() async {
    if (_appStore.isNewLogin) {
      final canUseFingerprint = await _biometricStore.canUseFingerprint;
      if (canUseFingerprint && !_settingStore.canFingerprint) {
        await Navigator.push(
            context, UpperRoute(page: const FingerprintSettingScreen()));
      }
      await _appStore.changeNewLogin(false); // Indicador de usuario logueado

    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    _mainTabBarController = TabController(length: 1, vsync: this);
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      _checkBiometricAuth();
    });
  }

  @override
  void dispose() {
    _mainPageViewController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: Scaffold(
        primary: true,
        extendBodyBehindAppBar: false,
        resizeToAvoidBottomInset: false,
        appBar: _BuildAppBar(
          appStore: _appStore,
          onMenuPressed: widget.onMenuPressed,
        ),
        body: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return ConstrainedBox(
              constraints: BoxConstraints(
                minWidth: constraints.maxWidth,
                minHeight: constraints.maxHeight,
              ),
              child: IntrinsicHeight(
                child: PageView(
                  scrollDirection: Axis.horizontal,
                  physics: const NeverScrollableScrollPhysics(),
                  controller: _mainPageViewController,
                  children: <Widget>[
                    _appStore.session?.userCodeType == 1
                        ? const HomeAdminScreen()
                        : const HomeVoterScreen()
                  ],
                ),
              ),
            );
          },
        ),
        bottomNavigationBar: _buildBottomNavigationBar(),
        backgroundColor: AppColor.primaryColor,
      ),
    );
  }

  Widget _buildBottomNavigationBar() {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.background,
        border: Border(
          top: BorderSide(
            color: AppColor.primaryColor,
            width: 1.0,
          ),
        ),
      ),
      child: TabBar(
        controller: _mainTabBarController,
        indicatorColor: AppColor.primaryColor,
        indicator: UnderlineTabIndicator(
          borderSide: BorderSide(color: AppColor.primaryColor, width: 1.0),
        ),
        indicatorSize: TabBarIndicatorSize.label,
        labelColor: AppColor.primaryColor,
        labelStyle:
            Theme.of(context).textTheme.bodySmall?.copyWith(fontSize: 12.0),
        unselectedLabelColor: Colors.grey,
        onTap: _onItemTapped,
        padding: EdgeInsets.zero,
        tabs: [
          Tab(
            child: _buildTap(
              'Inicio',
              CupertinoIcons.home,
            ),
            height: 56.0,
          ),
        ],
      ),
    );
  }

  _buildTap(String text, IconData iconData) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(bottom: 10.0),
          child: Icon(iconData, size: 18.0),
        ),
        Text(
          text,
          style:
              Theme.of(context).textTheme.bodySmall?.copyWith(fontSize: 12.0),
        ),
      ],
    );
  }
}

class _BuildAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  final Size preferredSize;
  final AppStore appStore;
  final VoidCallback? onMenuPressed;

  const _BuildAppBar({
    Key? key,
    required this.appStore,
    required this.onMenuPressed,
  })  : preferredSize = const Size.fromHeight(Ui.kToolbarHeight),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return EncuestaAppBar(
      color: AppColor.primaryColor,
      leading: const SizedBox.shrink(),
      leadingWidth: 0.0,
      actions: [
        IconButton(
          icon: const Icon(Icons.menu),
          onPressed: onMenuPressed,
        ),
      ],
      child: SizedBox(
        width: 236, //30
        height: Ui.kToolbarHeight,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Flexible(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      const Icon(Icons.person, size: 18.0),
                      const SizedBox(width: 4.0),
                      Observer(
                        builder: (context) {
                          final personName =
                              (appStore.session?.userName ?? '').capitalize();
                          return personName.isOverLength(20)
                              ? ConstrainedBox(
                                  constraints: const BoxConstraints(
                                    minHeight: 24.0,
                                    minWidth: 170.0,
                                    maxHeight: 24.0,
                                    maxWidth: 170.0,
                                  ),
                                  child: Marquee(
                                    text: personName,
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyMedium
                                        ?.copyWith(
                                            color: Colors.white,
                                            fontSize: 12.0),
                                    blankSpace: 20,
                                    velocity: 20.0,
                                    pauseAfterRound: const Duration(seconds: 5),
                                  ),
                                )
                              : Text(
                                  personName,
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyMedium
                                      ?.copyWith(
                                          color: Colors.white, fontSize: 12.0),
                                );
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
