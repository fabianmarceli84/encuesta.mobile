import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:app_mobile_encuesta/core/stores/app/app_store.dart';
import 'package:app_mobile_encuesta/core/stores/setting/setting_store.dart';
import 'package:app_mobile_encuesta/core/stores/theme/theme_store.dart';
import 'package:app_mobile_encuesta/di/components/service_locator.dart';
import 'package:app_mobile_encuesta/ui/widgets/empty_app_bar.dart';
import 'package:app_mobile_encuesta/ui/widgets/progress_indicator_widget.dart';
import 'package:app_mobile_encuesta/utils/transitions/page_transitions.dart';
import 'package:flutter/material.dart';

class LogoutScreen extends StatefulWidget {
  final Widget nextPage;

  const LogoutScreen({
    Key? key,
    required this.nextPage,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LogoutScreenState();
}

class _LogoutScreenState extends State<LogoutScreen> {
  final ThemeStore _themeStore = getIt<ThemeStore>();
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      await _navigate();
    });
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      primary: false,
      resizeToAvoidBottomInset: false,
      appBar: const EmptyAppBar(),
      body: SafeArea(
        top: false,
        child: _buildBody(),
      ),
    );
  }

  Widget _buildBody() {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return ConstrainedBox(
          constraints: BoxConstraints(
            minWidth: constraints.maxWidth,
            minHeight: constraints.maxHeight,
          ),
          child: IntrinsicHeight(
            child: Stack(
              fit: StackFit.expand,
              alignment: AlignmentDirectional.topCenter,
              children: [
                CustomProgressIndicatorWidget(
                  showShadow: false,
                  backgroundColor: _themeStore.darkMode
                      ? AppColor.backgroundDark
                      : AppColor.backgroundLight,
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Future<void> _navigate() async {
    //Lazy Singleton
    await getIt.resetLazySingleton<AppStore>(
        disposingFunction: (store) => store.logout());
    await getIt.resetLazySingleton<SettingStore>(
        disposingFunction: (store) => store.logout());

    //Singleton
    // getIt<AssistanceTypeStore>().logout();

    //Timer
    Future.delayed(const Duration(milliseconds: 1500), () {
      Navigator.pushAndRemoveUntil(
        context,
        RightRoute(page: widget.nextPage),
        (Route<dynamic> route) => false,
      );
    });
  }
}
