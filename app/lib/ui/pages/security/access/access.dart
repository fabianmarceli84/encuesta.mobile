// ignore_for_file: must_be_immutable

import 'dart:developer';
import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:app_mobile_encuesta/core/stores/biometric/biometric_store.dart';
import 'package:app_mobile_encuesta/core/stores/setting/setting_store.dart';
import 'package:app_mobile_encuesta/core/stores/theme/theme_store.dart';
import 'package:app_mobile_encuesta/di/components/service_locator.dart';
import 'package:app_mobile_encuesta/ui/pages/security/access/biometric/fingerprint_setting.dart';
import 'package:app_mobile_encuesta/ui/widgets/encuesta_app_bar.dart';
import 'package:app_mobile_encuesta/utils/transitions/page_transitions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class AccessScreen extends StatefulWidget {
  final String title;
  const AccessScreen({Key? key, this.title = 'Acceso'}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AccessScreenState();
}

class _AccessScreenState extends State<AccessScreen> {
  final ThemeStore _themeStore = getIt<ThemeStore>();
  final SettingStore _settingStore = getIt<SettingStore>();
  final BiometricStore _biometricStore = getIt<BiometricStore>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    _biometricStore.checkBiometrics();
    WidgetsBinding.instance.addPostFrameCallback((_) async {});
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => true,
      child: SafeArea(
        top: false,
        child: Scaffold(
          primary: true,
          extendBodyBehindAppBar: false,
          resizeToAvoidBottomInset: false,
          appBar: EncuestaAppBar(
            child: Text(widget.title),
          ),
          body: _buildBody(),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Material(
          color: _themeStore.darkMode
              ? AppColor.backgroundDark
              : AppColor.backgroundLight,
          child: Stack(
            fit: StackFit.expand,
            alignment: AlignmentDirectional.topCenter,
            children: [
              _buildContent(constraints),
            ],
          ),
        );
      },
    );
  }

  Widget _buildContent(BoxConstraints constraints) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Huella digital',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              _buildFingerprintSwitch(constraints),
            ],
          ),
          const Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Login automático',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              _buildAutomaticLoginSwitch(constraints),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildFingerprintSwitch(BoxConstraints constraints) {
    return Observer(
      builder: (context) {
        if (_biometricStore.biometricsAvailable) {
          return CupertinoSwitch(
            value: _settingStore.canFingerprint,
            onChanged: (value) async {
              if (value) {
                bool result = await Navigator.push(
                  context,
                  UpperRoute(page: const FingerprintSettingScreen()),
                );
                if (result) {
                  await _settingStore.changeAutomaticLogin(false);
                }
              } else {
                await _settingStore.changeFingerprintAuth(false);
              }
            },
          );
        } else {
          return const SizedBox.shrink();
        }
      },
    );
  }

  Widget _buildAutomaticLoginSwitch(BoxConstraints constraints) {
    return Observer(
      builder: (context) {
        return CupertinoSwitch(
          value: _settingStore.automaticLogin,
          onChanged: (value) async {
            log("$value");
            if (value) {
              _openAutomaticLogin();
            } else {
              _settingStore.changeAutomaticLogin(false);
            }
          },
        );
      },
    );
  }

  void _openAutomaticLogin() async {
    _openDialog(
      "Confirmación",
      Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: const [
            Text(
              '¿Estás seguro de activar esta función?',
              style: TextStyle(fontWeight: FontWeight.w500),
              textAlign: TextAlign.start,
            ),
            SizedBox(height: 4.0),
            Text(
              'Cualquier persona con acceso a tu dispositivo podrá ingresar al aplicativo.',
              textAlign: TextAlign.start,
            ),
          ],
        ),
      ),
      onAgree: () async {
        await _settingStore.changeAutomaticLogin(true);
        await _settingStore.changeFingerprintAuth(false);
      },
    );
  }

  void _openDialog(
    String title,
    Widget content, {
    VoidCallback? onAgree,
  }) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (_) {
        return AlertDialog(
          contentPadding: const EdgeInsets.all(6.0),
          title: Text(title),
          content: content,
          actions: [
            TextButton(
              onPressed: Navigator.of(context).pop,
              child: const Text('Cancelar'),
            ),
            TextButton(
              child: const Text('Aceptar'),
              onPressed: () {
                Navigator.of(context).pop();
                if (onAgree != null) onAgree.call();
              },
            ),
          ],
        );
      },
    );
  }
}
