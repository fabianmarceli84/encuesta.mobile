// ignore_for_file: use_build_context_synchronously

import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:app_mobile_encuesta/constants/ui.dart';
import 'package:app_mobile_encuesta/core/stores/biometric/biometric_store.dart';
import 'package:app_mobile_encuesta/core/stores/setting/setting_store.dart';
import 'package:app_mobile_encuesta/core/stores/theme/theme_store.dart';
import 'package:app_mobile_encuesta/di/components/service_locator.dart';
import 'package:app_mobile_encuesta/ui/widgets/empty_app_bar.dart';
import 'package:app_mobile_encuesta/ui/widgets/progress_indicator_widget.dart';
import 'package:app_mobile_encuesta/utils/message/message.dart';
import 'package:app_mobile_encuesta/utils/style/button_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_svg/flutter_svg.dart';

class FingerprintSettingScreen extends StatefulWidget {
  const FingerprintSettingScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FingerprintSettingScreenState();
}

class _FingerprintSettingScreenState extends State<FingerprintSettingScreen> {
  final ThemeStore _themeStore = getIt<ThemeStore>();
  final BiometricStore _biometricStore = getIt<BiometricStore>();
  final SettingStore _settingStore = getIt<SettingStore>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: SafeArea(
        top: false,
        child: Scaffold(
          primary: true,
          extendBodyBehindAppBar: true,
          resizeToAvoidBottomInset: false,
          appBar: const EmptyAppBar(),
          body: _buildBody(),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Material(
          color: _themeStore.darkMode
              ? AppColor.backgroundDark
              : AppColor.backgroundLight,
          child: Stack(
            alignment: AlignmentDirectional.topCenter,
            children: [
              _buildContent(constraints),
              _buildViewState(constraints),
              _buildLoading(constraints),
            ],
          ),
        );
      },
    );
  }

  Widget _buildViewState(BoxConstraints constraints) {
    return Observer(
      builder: (context) {
        return _biometricStore.success
            ? _save(changeFingerprintAuth: true)
            : Message.showErrorMessage(
                context,
                _biometricStore.errorStore.errorMessage,
              );
      },
    );
  }

  Widget _buildLoading(BoxConstraints constraints) {
    return Observer(
      builder: (context) {
        return Visibility(
          visible: _settingStore.loading,
          child: const CustomProgressIndicatorWidget(),
        );
      },
    );
  }

  Widget _buildContent(BoxConstraints constraints) {
    return Column(
      children: [
        _buildAppBar(constraints),
        const SizedBox(height: 20.0),
        _buildIcon(constraints),
        const SizedBox(height: 20.0),
        _buildTitle(constraints),
        const SizedBox(height: 10.0),
        _buildDescription(constraints),
        const Expanded(child: SizedBox()),
        _buildButtons(constraints),
      ],
    );
  }

  Widget _buildAppBar(BoxConstraints constraints) {
    var height = MediaQuery.of(context).viewPadding.top;
    return Container(
      color: Theme.of(context).colorScheme.primary,
      height: height + Ui.kToolbarHeight,
      child: Padding(
        padding: EdgeInsets.only(top: height),
        child: Center(
          child: Text(
            "Modo de ingreso",
            textAlign: TextAlign.center,
            style: Theme.of(context)
                .textTheme
                .titleLarge
                ?.copyWith(color: Colors.white),
          ),
        ),
      ),
    );
  }

  Widget _buildIcon(BoxConstraints constraints) {
    return SvgPicture.asset('assets/icons/fingerprint2.svg',
        semanticsLabel: 'Fingerprint', height: 120);
  }

  Widget _buildTitle(BoxConstraints constraints) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Text("Activa tu modo de ingreso con tu huella digital.",
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.titleLarge),
    );
  }

  Widget _buildDescription(BoxConstraints constraints) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Text(
          "Ingresa de forma rápida y disfruta de una mejor experiencia al utilizar tu App Encuesta.",
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodySmall),
    );
  }

  Widget _buildButtonOk(BoxConstraints constraints) {
    return ElevatedButton(
      style: EncuestaButtonStyle.primary,
      child: Text('Activar', style: Theme.of(context).textTheme.button),
      onPressed: () {
        _startBiometric();
      },
    );
  }

  Widget _buildButtonSkip(BoxConstraints constraints) {
    return TextButton(
      style: EncuestaButtonStyle.primary,
      onPressed: () => _save(changeFingerprintAuth: false),
      child: Text("Omitir",
          style: Theme.of(context)
              .textTheme
              .button
              ?.copyWith(color: AppColor.primaryColor)),
    );
  }

  Widget _buildButtons(BoxConstraints constraints) {
    return SizedBox(
      width: 150.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          _buildButtonOk(constraints),
          _buildButtonSkip(constraints),
        ],
      ),
    );
  }

  _startBiometric() async {
    final canUseFingerprint = await _biometricStore.canUseFingerprint;
    if (canUseFingerprint) {
      _biometricStore.authenticate();
    } else {
      Message.showErrorMessage(
        context,
        'No se detectó autenticación biométrica',
      );
    }
  }

  _save({bool changeFingerprintAuth = false}) {
    Future.delayed(const Duration(milliseconds: 0), () async {
      await _settingStore.changeFingerprintAuth(changeFingerprintAuth);
      Navigator.pop(context, changeFingerprintAuth);
    });

    return const SizedBox.shrink();
  }
}
