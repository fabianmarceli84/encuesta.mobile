// ignore_for_file: must_be_immutable

import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:app_mobile_encuesta/core/model/custom_item.dart';
import 'package:app_mobile_encuesta/core/stores/theme/theme_store.dart';
import 'package:app_mobile_encuesta/di/components/service_locator.dart';
import 'package:app_mobile_encuesta/ui/pages/setting/style/style.dart';
import 'package:app_mobile_encuesta/ui/widgets/encuesta_app_bar.dart';
import 'package:app_mobile_encuesta/utils/transitions/page_transitions.dart';
import 'package:flutter/material.dart';

class SettingScreen extends StatefulWidget {
  final String title;
  const SettingScreen({Key? key, this.title = 'Configuración'})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _SettingScreenState();
}

class _SettingScreenState extends State<SettingScreen> {
  final ThemeStore _themeStore = getIt<ThemeStore>();

  var settingList = <CustomItem>[];

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {});
    settingList = <CustomItem>[
      CustomItem(
        iconData: Icons.style_outlined,
        label: 'Estilo',
        nextPage: const StyleScreen(),
      ),
    ];
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => true,
      child: SafeArea(
        top: false,
        child: Scaffold(
          primary: true,
          extendBodyBehindAppBar: false,
          resizeToAvoidBottomInset: false,
          appBar: EncuestaAppBar(
            child: Text(widget.title),
          ),
          body: _buildBody(),
          backgroundColor: _themeStore.darkMode
              ? AppColor.backgroundDark
              : AppColor.backgroundLight,
        ),
      ),
    );
  }

  Widget _buildBody() {
    return LayoutBuilder(
      builder: (context, constraints) {
        return ConstrainedBox(
          constraints: BoxConstraints(
            minWidth: constraints.maxWidth,
            minHeight: constraints.maxHeight,
          ),
          child: IntrinsicHeight(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildResultList(constraints),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _buildResultList(BoxConstraints constraints) {
    return Flexible(
      child: Stack(
        children: [
          _buildList(constraints),
        ],
      ),
    );
  }

  Widget _buildList(BoxConstraints constraints) {
    return NotificationListener<OverscrollIndicatorNotification>(
      onNotification: (overscroll) {
        overscroll.disallowIndicator();
        return true;
      },
      child: ListView.separated(
        itemCount: settingList.length,
        physics: const AlwaysScrollableScrollPhysics(),
        separatorBuilder: (context, position) {
          return const Divider(height: 0.5, thickness: 0.5);
        },
        itemBuilder: (context, position) {
          return _buildItem(position);
        },
      ),
    );
  }

  Widget _buildItem(int position) {
    final item = settingList[position];

    return InkWell(
      onTap: () => _navigateSubMenu(item.nextPage),
      child: Material(
        borderRadius: BorderRadius.zero,
        color: Colors.transparent,
        child: SizedBox(
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
            child: Row(
              children: [
                Icon(item.iconData, size: 30.0, color: Colors.grey),
                const SizedBox(width: 10.0),
                Text(item.label,
                    style: Theme.of(context).textTheme.titleMedium),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _navigateSubMenu(Widget nextPage) {
    Navigator.push(
      context,
      LeftRoute(
        page: nextPage,
      ),
    );
  }
}
