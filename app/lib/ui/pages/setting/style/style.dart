// ignore_for_file: must_be_immutable

import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:app_mobile_encuesta/core/stores/theme/theme_store.dart';
import 'package:app_mobile_encuesta/di/components/service_locator.dart';
import 'package:app_mobile_encuesta/ui/widgets/encuesta_app_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_material_color_picker/flutter_material_color_picker.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

class StyleScreen extends StatefulWidget {
  final String title;
  const StyleScreen({Key? key, this.title = 'Estilo'}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _StyleScreenState();
}

class _StyleScreenState extends State<StyleScreen> {
  final ThemeStore _themeStore = getIt<ThemeStore>();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {});
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => true,
      child: SafeArea(
        top: false,
        child: Scaffold(
          primary: true,
          extendBodyBehindAppBar: false,
          resizeToAvoidBottomInset: false,
          appBar: EncuestaAppBar(
            child: Text(widget.title),
          ),
          body: _buildBody(),
        ),
      ),
    );
  }

  Widget _buildBody() {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Material(
          color: _themeStore.darkMode
              ? AppColor.backgroundDark
              : AppColor.backgroundLight,
          child: Stack(
            fit: StackFit.expand,
            alignment: AlignmentDirectional.topCenter,
            children: [
              _buildContent(constraints),
            ],
          ),
        );
      },
    );
  }

  Widget _buildContent(BoxConstraints constraints) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Modo oscuro',
                  style: Theme.of(context).textTheme.titleMedium),
              _buildThemeSwitch(constraints),
            ],
          ),
          const Divider(),
          Text('Experimental', style: Theme.of(context).textTheme.bodySmall),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Pantalla completa',
                  style: Theme.of(context).textTheme.titleMedium),
              _buildFullScreenSwitch(constraints),
            ],
          ),
          const Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Color', style: Theme.of(context).textTheme.titleMedium),
              _buildPrimaryColorSwitch(constraints),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildThemeSwitch(BoxConstraints constraints) {
    return Observer(
      builder: (context) {
        return CupertinoSwitch(
          value: _themeStore.darkMode,
          onChanged: (value) async {
            await _themeStore.changeBrightnessToDark(value);
          },
        );
      },
    );
  }

  Widget _buildFullScreenSwitch(BoxConstraints constraints) {
    return Observer(
      builder: (context) {
        return CupertinoSwitch(
          value: _themeStore.fullScreenMode,
          onChanged: (value) async {
            await _themeStore.changeFullScreenMode(value);
          },
        );
      },
    );
  }

  Widget _buildPrimaryColorSwitch(BoxConstraints constraints) {
    return Observer(
      builder: (context) {
        return Material(
          color: Color(_themeStore.primaryColor),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          child: InkWell(
            borderRadius: BorderRadius.circular(15),
            onTap: _openMaterialColorPicker,
            child: const SizedBox(width: 30.0, height: 30.0),
          ),
        );
      },
    );
  }

  void _openMaterialColorPicker() async {
    _openDialog(
      "Material Color",
      MaterialColorPicker(
        allowShades: false,
        colors: AppColor.materialColors,
        selectedColor: Color(_themeStore.primaryColor),
        onMainColorChange: (color) async {
          await _themeStore.changePrimaryColor(color!.value);
        },
      ),
    );
  }

  void _openDialog(String title, Widget content) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (_) {
        return AlertDialog(
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(.0)),
          ),
          contentPadding: const EdgeInsets.all(6.0),
          title: Text(title),
          content: content,
          actions: [
            TextButton(
              onPressed: Navigator.of(context).pop,
              child: const Text('Cancelar'),
            ),
            TextButton(
              child: const Text('Aceptar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
