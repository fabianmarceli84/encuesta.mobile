// ignore_for_file: use_build_context_synchronously

import 'package:animate_do/animate_do.dart';
import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:app_mobile_encuesta/constants/ui.dart';
import 'package:app_mobile_encuesta/core/stores/app/app_store.dart';
import 'package:app_mobile_encuesta/core/stores/biometric/biometric_store.dart';
import 'package:app_mobile_encuesta/core/stores/login/login_store.dart';
import 'package:app_mobile_encuesta/core/stores/setting/setting_store.dart';
import 'package:app_mobile_encuesta/core/stores/theme/theme_store.dart';
import 'package:app_mobile_encuesta/di/components/service_locator.dart';
import 'package:app_mobile_encuesta/ui/pages/login_auth/login_unlock.dart';
import 'package:app_mobile_encuesta/ui/pages/main/main_drawer.dart';
import 'package:app_mobile_encuesta/ui/widgets/empty_app_bar.dart';
import 'package:app_mobile_encuesta/ui/widgets/encuesta_app_bar.dart';
import 'package:app_mobile_encuesta/ui/widgets/show_hide_eye.dart';
import 'package:app_mobile_encuesta/ui/widgets/text_form_field_custom.dart';
import 'package:app_mobile_encuesta/utils/extension/extensions.dart';
import 'package:app_mobile_encuesta/utils/message/message.dart';
import 'package:app_mobile_encuesta/utils/style/button_style.dart';
import 'package:app_mobile_encuesta/utils/validators/validators.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lottie/lottie.dart';

class LoginAuthScreen extends StatefulWidget {
  const LoginAuthScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LoginAuthScreenState();
}

class _LoginAuthScreenState extends State<LoginAuthScreen> {
  final ThemeStore _themeStore = getIt<ThemeStore>();
  final AppStore _appStore = getIt<AppStore>();
  final SettingStore _settingStore = getIt<SettingStore>();
  final BiometricStore _biometricStore = getIt<BiometricStore>();
  final LoginStore _loginStore = getIt<LoginStore>();

  final TextEditingController _passwordTextController = TextEditingController();

  _checkLogin() {
    if (_settingStore.automaticLogin) {
      _navigateBiometric();
    } else {
      _checkBiometricAuth();
    }
  }

  _checkBiometricAuth() async {
    if (_settingStore.canFingerprint) {
      final canUseFingerprint = await _biometricStore.canUseFingerprint;
      if (canUseFingerprint) {
        final didAuthenticate = await _biometricStore.authenticate();
        if (didAuthenticate) {
          _navigateBiometric();
        } else {
          final isFraud = await _settingStore.isFraudFingerprintAuth();
          if (isFraud) {
            Message.showErrorMessage(
              context,
              'Se bloqueó la autenticación biométrica, vuelva a activarlo ingresando.',
            );
          } else {
            Message.showErrorMessage(
              context,
              'Falló la autenticación biométrica, vuelva a intentar. Te queda ${_settingStore.limitErrorAmount - _settingStore.errorAmount} intento(s)',
            );
          }
        }
      } else {
        await _settingStore.changeFingerprintAuth(false);
        Message.showErrorMessage(
          context,
          'No se detectó autenticación biométrica.',
        );
      }
    }
  }

  _navigate() async {
    final session = await _appStore.currentSession;
    _loginStore.setUsername(session!.userName);
    if (_loginStore.canLogin) {
      _loginStore.loginAuth(
        session,
        isBiometric: false,
      );
    } else {
      Message.showErrorMessage(
        context,
        'Complete los campos vacíos',
      );
    }
  }

  _navigateBiometric() async {
    final session = await _appStore.currentSession;
    _loginStore.loginAuth(
      session!,
      isBiometric: true,
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      _checkLogin();
    });
  }

  @override
  void dispose() {
    _passwordTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: Scaffold(
        primary: true,
        extendBodyBehindAppBar: true,
        resizeToAvoidBottomInset: false,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(Ui.kToolbarHeight),
          child: Observer(
            builder: (context) {
              return _loginStore.success
                  ? const EmptyAppBar()
                  : EncuestaAppBar(color: Colors.transparent);
            },
          ),
        ),
        body: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return Material(
              color: Colors.transparent,
              child: Stack(
                alignment: AlignmentDirectional.topCenter,
                children: [
                  _BuilWallPaperBackgroud(
                    constraints: constraints,
                  ),
                  _BuildContent(
                    constraints: constraints,
                    themeStore: _themeStore,
                    builForm: _BuilForm(
                      biometricStore: _biometricStore,
                      themeStore: _themeStore,
                      loginStore: _loginStore,
                      appStore: _appStore,
                      passwordTextController: _passwordTextController,
                      navigate: _navigate,
                      buildButtonBiometric: _BuildButtonBiometric(
                        settingStore: _settingStore,
                        biometricStore: _biometricStore,
                        onPressed: _checkBiometricAuth,
                        themeStore: _themeStore,
                      ),
                    ),
                  ),
                  _BuildViewState(
                    loginStore: _loginStore,
                    biometricStore: _biometricStore,
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}

class _BuilWallPaperBackgroud extends StatelessWidget {
  final BoxConstraints constraints;

  const _BuilWallPaperBackgroud({
    Key? key,
    required this.constraints,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: constraints.maxHeight * 0.225,
      width: constraints.maxWidth,
      child: Lottie.asset(
        'assets/lottie/login.json',
        repeat: true,
        animate: true,
      ),
    );
  }
}

class _BuildContent extends StatelessWidget {
  final BoxConstraints constraints;
  final ThemeStore themeStore;
  final Widget builForm;

  const _BuildContent({
    Key? key,
    required this.constraints,
    required this.themeStore,
    required this.builForm,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: constraints.maxHeight * 0.225),
        Expanded(
          child: FadeIn(
            child: Container(
              width: constraints.maxWidth,
              color: themeStore.darkMode
                  ? AppColor.backgroundDark
                  : AppColor.backgroundLight,
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: builForm,
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class _BuilForm extends StatelessWidget {
  final BiometricStore biometricStore;
  final ThemeStore themeStore;
  final LoginStore loginStore;
  final AppStore appStore;
  final TextEditingController passwordTextController;
  final VoidCallback navigate;
  final Widget buildButtonBiometric;

  const _BuilForm({
    Key? key,
    required this.biometricStore,
    required this.themeStore,
    required this.loginStore,
    required this.appStore,
    required this.passwordTextController,
    required this.navigate,
    required this.buildButtonBiometric,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (context) {
        return biometricStore.loading
            ? Column(
                children: [
                  const SizedBox(height: 20),
                  Lottie.asset(
                    themeStore.darkMode
                        ? 'assets/lottie/fingerprint_white_success.json'
                        : 'assets/lottie/fingerprint_black_success.json',
                    repeat: true,
                    animate: true,
                    height: 50,
                  ),
                  const SizedBox(height: 4),
                  Text(
                    "Ingresar con\n huella digital",
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.bodyMedium,
                  ),
                ],
              )
            : (biometricStore.success ||
                    loginStore.loading ||
                    loginStore.success)
                ? Column(
                    children: [
                      const SizedBox(height: 20),
                      Center(
                        child: CircularProgressIndicator(
                          color:
                              themeStore.darkMode ? Colors.white : Colors.black,
                        ),
                      ),
                      const SizedBox(height: 20),
                      Text(
                        "Verificando",
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.bodyMedium,
                      ),
                    ],
                  )
                : Column(
                    children: [
                      const SizedBox(height: 8),
                      Text(
                        (appStore.session?.userName ?? '').capitalize(),
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      const SizedBox(height: 20),
                      Observer(
                        builder: (context) {
                          return TextFormFieldCustom(
                            textController: passwordTextController,
                            labelText: 'Clave',
                            regExp: Validators.textCleanPattern,
                            suffixIcon: ShowHideEye(
                              foregroundColor: themeStore.darkMode
                                  ? Colors.white
                                  : Colors.black,
                              obscure: loginStore.obscure,
                              onPressed: (obscure) {
                                loginStore.setObscure(obscure);
                              },
                            ),
                            isObscure: loginStore.obscure,
                            maxLength: 30,
                            errorText: loginStore.formErrorStore.password,
                            onChanged: (value) {
                              loginStore
                                  .setPassword(passwordTextController.text);
                            },
                          );
                        },
                      ),
                      const SizedBox(height: 30),
                      ElevatedButton(
                        style: EncuestaButtonStyle.primary,
                        onPressed: (biometricStore.loading ||
                                loginStore.loading ||
                                loginStore.success)
                            ? null
                            : navigate,
                        child: const Text('Ingresar'),
                      ),
                      const Expanded(child: SizedBox()),
                      buildButtonBiometric,
                    ],
                  );
      },
    );
  }
}

class _BuildButtonBiometric extends StatelessWidget {
  final SettingStore settingStore;
  final BiometricStore biometricStore;
  final VoidCallback onPressed;
  final ThemeStore themeStore;

  const _BuildButtonBiometric({
    Key? key,
    required this.settingStore,
    required this.biometricStore,
    required this.onPressed,
    required this.themeStore,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (context) {
        return settingStore.canFingerprint
            ? FutureBuilder<bool>(
                future: biometricStore
                    .canUseBiometrics, // a previously-obtained Future<String> or null
                builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
                  if (snapshot.hasData) {
                    if (snapshot.data!) {
                      return TextButton.icon(
                        onPressed: onPressed,
                        label: Text("Ingresar con huella digital",
                            style: Theme.of(context).textTheme.bodyMedium),
                        icon: SvgPicture.asset(
                          'assets/icons/fingerprint.svg',
                          semanticsLabel: 'Fingerprint',
                          height: 24,
                          color:
                              themeStore.darkMode ? Colors.white : Colors.black,
                        ),
                      );
                    }
                  }
                  return const SizedBox();
                },
              )
            : const SizedBox.shrink();
      },
    );
  }
}

class _BuildViewState extends StatelessWidget {
  final LoginStore loginStore;
  final BiometricStore biometricStore;

  const _BuildViewState({
    Key? key,
    required this.loginStore,
    required this.biometricStore,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (context) {
        return loginStore.success
            ? const LoginUnlockScreen(nextPage: MainDrawerScreen())
            : Message.showErrorMessage(
                context,
                biometricStore.errorStore.errorMessage.isEmpty
                    ? loginStore.errorStore.errorMessage
                    : biometricStore.errorStore.errorMessage,
              );
      },
    );
  }
}
