import 'dart:developer';
import 'package:animate_do/animate_do.dart';
import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:app_mobile_encuesta/core/stores/theme/theme_store.dart';
import 'package:app_mobile_encuesta/di/components/service_locator.dart';
import 'package:app_mobile_encuesta/ui/widgets/empty_app_bar.dart';
import 'package:app_mobile_encuesta/utils/transitions/page_transitions.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class LoginUnlockScreen extends StatefulWidget {
  final Widget nextPage;

  const LoginUnlockScreen({Key? key, required this.nextPage}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LoginUnlockScreenState();
}

class _LoginUnlockScreenState extends State<LoginUnlockScreen>
    with TickerProviderStateMixin {
  final ThemeStore _themeStore = getIt<ThemeStore>();

  late final AnimationController _controller;
  late final DateTime strDate;
  int _flex0 = 3, _flex1 = 0, _flex2 = 0;
  int _animationStep = 0;

  _stopWatch() {
    DateTime endDate = DateTime.now();
    final duration = (endDate.difference(strDate).inMilliseconds).round();
    log('duration: ${duration}ms');
  }

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(vsync: this);
    _controller.addStatusListener((AnimationStatus status) async {
      if (status == AnimationStatus.completed && _controller.value == 1) {
        log('completed: ${_controller.value}');
        _stopWatch();
        log('push');
        _navigate();
      }
    });
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      strDate = DateTime.now();
      log('animation start step 1');
      if (mounted) {
        setState(() {
          _flex0 = 0;
          _flex1 = 2;
          _flex2 = 1;
          _animationStep = 1;
        });
      }
    });
  }

  @override
  dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      primary: false,
      resizeToAvoidBottomInset: false,
      appBar: const EmptyAppBar(),
      body: SafeArea(
        top: false,
        child: _buildBody(),
      ),
      backgroundColor: Colors.transparent,
    );
  }

  Widget _buildBody() {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        const total = 3;
        final height = constraints.maxHeight;
        final height0 = (height * _flex0) / total;
        final height1 = (height * _flex1) / total;
        final height2 = (height * _flex2) / total;
        return Material(
          color: Colors.transparent,
          child: Column(
            children: [
              AnimatedContainer(
                height: height0,
                duration: const Duration(milliseconds: 200),
                color: Colors.transparent,
                onEnd: () async {
                  if (_animationStep == 1) {
                    await Future.delayed(const Duration(milliseconds: 800));
                    log('animation start step 2');
                    if (mounted) {
                      setState(() {
                        _flex0 = 0;
                        _flex1 = 0;
                        _flex2 = 3;
                        _animationStep = 2;
                      });
                    }
                  }
                },
              ),
              AnimatedContainer(
                height: height1,
                duration: const Duration(milliseconds: 200),
                color: _themeStore.darkMode
                    ? AppColor.backgroundDark
                    : AppColor.backgroundLight,
                onEnd: () async {
                  if (_animationStep == 2) {
                    await Future.delayed(const Duration(milliseconds: 200));
                    log('animation start step 3');
                    if (mounted) {
                      setState(() {
                        _animationStep = 3;
                      });
                    }
                  }
                },
                child: Center(
                  child: FadeOut(
                    delay: const Duration(milliseconds: 1200),
                    animate: true,
                    child:
                        CircularProgressIndicator(color: AppColor.primaryColor),
                  ),
                ),
              ),
              AnimatedContainer(
                height: height2,
                duration: const Duration(milliseconds: 200),
                color: _animationStep == 3
                    ? Colors.amber //0xFF19C670
                    : AppColor.primaryColor,
                onEnd: () async {
                  if (_animationStep == 3) {
                    log('animation start step 4');
                    _animationStep = 4;
                    await _controller.forward(from: 0.26);
                  }
                },
                child: Center(
                  child: FadeIn(
                    delay: const Duration(milliseconds: 200),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Lottie.asset(
                          'assets/lottie/unlocked.json',
                          repeat: false,
                          animate: false,
                          controller: _controller,
                          onLoaded: (composition) {
                            _controller.duration =
                                const Duration(milliseconds: 1000);
                            _controller.animateTo(0.26);
                          },
                          height: 70,
                        ),
                        const SizedBox(height: 8),
                        FadeOut(
                          delay: const Duration(milliseconds: 1200),
                          animate: true,
                          child: Text(
                            "Estableciendo conexión segura",
                            style: Theme.of(context)
                                .textTheme
                                .button
                                ?.copyWith(color: Colors.white),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  _navigate() {
    Future.delayed(const Duration(milliseconds: 0), () {
      if (!mounted) return;
      Navigator.pushAndRemoveUntil(context, LeftRoute(page: widget.nextPage),
          (Route<dynamic> route) => false);
    });
  }
}
