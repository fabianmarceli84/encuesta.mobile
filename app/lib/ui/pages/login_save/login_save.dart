import 'package:animate_do/animate_do.dart';
import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:app_mobile_encuesta/core/stores/app/app_store.dart';
import 'package:app_mobile_encuesta/core/stores/setting/setting_store.dart';
import 'package:app_mobile_encuesta/core/stores/theme/theme_store.dart';
import 'package:app_mobile_encuesta/di/components/service_locator.dart';
import 'package:app_mobile_encuesta/ui/pages/login/login.dart';
import 'package:app_mobile_encuesta/ui/pages/login_auth/login_auth.dart';
import 'package:app_mobile_encuesta/ui/pages/logout/logout.dart';
import 'package:app_mobile_encuesta/ui/widgets/encuesta_app_bar.dart';
import 'package:app_mobile_encuesta/utils/extension/extensions.dart';
import 'package:app_mobile_encuesta/utils/style/button_style.dart';
import 'package:app_mobile_encuesta/utils/transitions/page_transitions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:lottie/lottie.dart';

class LoginSaveScreen extends StatefulWidget {
  const LoginSaveScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LoginSaveScreenState();
}

class _LoginSaveScreenState extends State<LoginSaveScreen> {
  final ThemeStore _themeStore = getIt<ThemeStore>();
  final AppStore _appStore = getIt<AppStore>();
  final SettingStore _settingStore = getIt<SettingStore>();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      _checkLogin();
    });
  }

  _checkLogin() {
    if (_settingStore.automaticLogin) {
      Navigator.push(
        context,
        LeftRoute(page: const LoginAuthScreen()),
      );
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      child: Scaffold(
        primary: true,
        extendBodyBehindAppBar: true,
        resizeToAvoidBottomInset: false,
        appBar: EncuestaAppBar(
          color: _themeStore.darkMode ? Colors.black : AppColor.primaryColor,
          colorLogo: _themeStore.darkMode ? Colors.white : Colors.white,
          onlyLogo: true,
          animate: true,
        ),
        body: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return Material(
              child: FadeIn(
                child: Stack(
                  alignment: AlignmentDirectional.topCenter,
                  children: [
                    _BuildWallPaperBackgroud(
                      constraints: constraints,
                    ),
                    _BuildWallPaperBackgroudFade(
                      constraints: constraints,
                      themeStore: _themeStore,
                    ),
                    _BuildCenter(
                      constraints: constraints,
                      themeStore: _themeStore,
                      appStore: _appStore,
                      buildConfirmationSnackbar: _BuildConfirmationSnackbar(
                        constraints: constraints,
                        themeStore: _themeStore,
                      ),
                    ),
                    _BuildLoginButton(
                      constraints: constraints,
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class _BuildWallPaperBackgroud extends StatelessWidget {
  final BoxConstraints constraints;

  const _BuildWallPaperBackgroud({
    Key? key,
    required this.constraints,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: constraints.maxHeight,
      width: constraints.maxWidth,
      child: Lottie.asset(
        'assets/lottie/login.json',
        repeat: true,
        animate: true,
      ),
    );
  }
}

class _BuildWallPaperBackgroudFade extends StatelessWidget {
  final BoxConstraints constraints;
  final ThemeStore themeStore;

  const _BuildWallPaperBackgroudFade({
    Key? key,
    required this.constraints,
    required this.themeStore,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: constraints.maxHeight,
      width: constraints.maxWidth,
      color: themeStore.darkMode ? Colors.black.withOpacity(0.2) : null,
    );
  }
}

class _BuildCenter extends StatelessWidget {
  final BoxConstraints constraints;
  final ThemeStore themeStore;
  final AppStore appStore;
  final Widget buildConfirmationSnackbar;

  const _BuildCenter({
    Key? key,
    required this.constraints,
    required this.themeStore,
    required this.appStore,
    required this.buildConfirmationSnackbar,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned.fill(
      child: Align(
        alignment: Alignment.center,
        child: Container(
          width: constraints.maxWidth,
          color: themeStore.darkMode
              ? Colors.black.withOpacity(0.8)
              : Colors.white.withOpacity(0.8),
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(height: 8),
                Text(
                  (appStore.session?.userName ?? '').capitalize(),
                  style: Theme.of(context).textTheme.headline6,
                  textAlign: TextAlign.center,
                ),
                TextButton(
                  onPressed: () {
                    showModalBottomSheet<void>(
                      context: context,
                      builder: (BuildContext context) {
                        return buildConfirmationSnackbar;
                      },
                    );
                  },
                  child: Text(
                    "Cambiar usuario",
                    style: Theme.of(context)
                        .textTheme
                        .button
                        ?.copyWith(color: Theme.of(context).primaryColor),
                    textAlign: TextAlign.center,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _BuildConfirmationSnackbar extends StatelessWidget {
  final BoxConstraints constraints;
  final ThemeStore themeStore;

  const _BuildConfirmationSnackbar({
    Key? key,
    required this.constraints,
    required this.themeStore,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        height: 220,
        decoration: BoxDecoration(
          color: themeStore.darkMode ? Colors.black : Colors.white,
          border: Border(
            top: BorderSide(
              width: 1.0,
              color: AppColor.primaryColor,
            ),
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              const SizedBox(height: 20),
              SvgPicture.asset(
                'assets/icons/info.svg',
                semanticsLabel: 'Info',
                height: 24,
              ),
              const SizedBox(height: 20),
              const Text(
                '¿Estás seguro de que quieres cambiar de usuario?',
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 20),
              SizedBox(
                width: 120,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    ElevatedButton(
                      style: EncuestaButtonStyle.primary,
                      child: Text(
                        'Cambiar',
                        style: Theme.of(context).textTheme.button,
                        textAlign: TextAlign.center,
                      ),
                      onPressed: () {
                        Navigator.pushAndRemoveUntil(
                            context,
                            RightRoute(
                              page: const LogoutScreen(
                                nextPage: LoginScreen(),
                              ),
                            ),
                            (Route<dynamic> route) => false);
                      },
                    ),
                    TextButton(
                      style: EncuestaButtonStyle.primary,
                      onPressed: () => Navigator.pop(context),
                      child: Text(
                        "Cancelar",
                        style: Theme.of(context)
                            .textTheme
                            .button
                            ?.copyWith(color: AppColor.primaryColor),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _BuildLoginButton extends StatelessWidget {
  final BoxConstraints constraints;

  const _BuildLoginButton({
    Key? key,
    required this.constraints,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: constraints.maxHeight * 0.45),
        SizedBox(
          height: constraints.maxHeight * 0.55,
          child: Center(
            child: ElevatedButton(
              style: EncuestaButtonStyle.primary,
              onPressed: () {
                Navigator.push(
                  context,
                  LeftRoute(page: const LoginAuthScreen()),
                );
              },
              child: const Text(
                'Iniciar sesión',
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
