import 'dart:io';
import 'package:app_mobile_encuesta/data/local/database/db.dart';
import 'package:app_mobile_encuesta/data/local/spf/shared_preference_helper.dart';
import 'package:app_mobile_encuesta/data/network/apis/encuesta/dto/survey/register_vote_request.dart';
import 'package:app_mobile_encuesta/data/network/apis/encuesta/dto/survey/register_vote_response.dart';
import 'package:app_mobile_encuesta/data/network/apis/encuesta/dto/survey/report_response.dart';
import 'package:app_mobile_encuesta/data/network/apis/encuesta/dto/survey/survey_response.dart';
import 'package:app_mobile_encuesta/data/network/apis/encuesta/dto/user/login_response.dart';
import 'package:app_mobile_encuesta/data/network/apis/encuesta/encuesta_api.dart';

class Repository {
  // data source object
  final EncuestaDatabase _encuestaDatabase;

  // api objects
  final EncuestaApi _encuestaApi;

  // shared pref object
  final SharedPreferenceHelper _sharedPrefsHelper;

  // constructor
  Repository(
    this._encuestaDatabase,
    this._sharedPrefsHelper,
    this._encuestaApi,
  );

  Future<LoginResponse> getLogin(
    String username,
    String password,
  ) async {
    return _encuestaApi.getLogin(
      username,
      password,
    );
  }

  Future<SurveyResponse> getSurvey(
    int userCode,
  ) async {
    return _encuestaApi.getSurvey(userCode);
  }

  Future<ReportResponse> getReport(
    int surveyCode,
  ) async {
    return _encuestaApi.getReport(surveyCode);
  }

  Future<RegisterVoteResponse> postRegisterVote(
    RegisterVoteRequest request,
  ) async {
    return _encuestaApi.postRegisterVote(request);
  }

  Future<void> exportDB(File file) async {
    return _encuestaDatabase.exportInto(file);
  }

  // SesionDao: ----------------------------------------------------------------
  Future<SesionData?> getLastSesion() {
    return _encuestaDatabase.sesionDao.getLast();
  }

  Future<int> insertSesion(SesionCompanion data) {
    return _encuestaDatabase.sesionDao.insert(data);
  }

  // Login SPF: ----------------------------------------------------------------
  bool get isLoggedIn => _sharedPrefsHelper.isLoggedIn;

  Future<void> saveIsLoggedIn(bool value) =>
      _sharedPrefsHelper.saveIsLoggedIn(value);

  bool get isNewLogin => _sharedPrefsHelper.isNewLogin;

  Future<void> changeNewLogin(bool value) =>
      _sharedPrefsHelper.changeNewLogin(value);

  // UUID: ---------------------------------------------------------------------
  String? get uuidMobile => _sharedPrefsHelper.uuidMobile;

  Future<void> saveUUIDMobile(String value) =>
      _sharedPrefsHelper.saveUUIDMobile(value);

  Future<void> removeUUIDMobile() => _sharedPrefsHelper.removeUUIDMobile();

  // Biometric auth: -----------------------------------------------------------
  bool get isFingerprintAuth => _sharedPrefsHelper.isFingerprintAuth;

  Future<void> changeFingerprintAuth(bool value) =>
      _sharedPrefsHelper.changeFingerprintAuth(value);

  bool get isAutomaticLogin => _sharedPrefsHelper.isAutomaticLogin;

  Future<void> changeAutomaticLogin(bool value) =>
      _sharedPrefsHelper.changeAutomaticLogin(value);

  // Theme: --------------------------------------------------------------------
  Future<void> changeBrightnessToDark(bool value) =>
      _sharedPrefsHelper.changeBrightnessToDark(value);

  bool get isDarkMode => _sharedPrefsHelper.isDarkMode;

  Future<void> changeFullScreenMode(bool value) =>
      _sharedPrefsHelper.changeFullScreenMode(value);

  bool get isFullScreenMode => _sharedPrefsHelper.isFullScreenMode;

  Future<void> changePrimaryColor(int value) =>
      _sharedPrefsHelper.changePrimaryColor(value);

  int get primaryColor => _sharedPrefsHelper.primaryColor;
}
