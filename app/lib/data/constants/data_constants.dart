// ignore_for_file: constant_identifier_names

class DBConstants {
  DBConstants._();

  // DB Name
  static const name = 'encuesta.sqlite';
}

class SecurityKeyConstants {
  SecurityKeyConstants._();

  static const key = '8uIJkYIsG2pX/AmfcWNfAg==';
  static const iv = 16;
}

class Preferences {
  Preferences._();

  static const String uuid_mobile = 'uuid_mobile';
  static const String is_logged_in = 'isLoggedIn';
  static const String is_new_login = 'is_new_login';
  static const String is_biometric = 'is_biometric';
  static const String is_automatic_login = 'is_automatic_login';
  static const String is_dark_mode = 'is_dark_mode';
  static const String is_fullscreen_mode = 'is_fullscreen_mode';
  static const String primary_color = 'primary_color';
}

class GenericErrorMessages {
  static const String database =
      'Algo ocurrió con la información, vuelva a intentarlo :(';
  static const String logic = 'Algo ocurrió, vuelva a intentarlo :(';
  static const String network =
      'Algo ocurrió con la red, vuelva a intentarlo :(';
  static const String server =
      'Algo ocurrió con el servidor, vuelva a intentarlo :(';
  static const String tokenError =
      'El servidor no respondió correctamente, vuelva a intentarlo.';
  static const String noInformation = 'No se encontraron registros.';
  static const String failedScan = 'No se pudo escanear.';
}

class NetworkFlags {
  NetworkFlags._();
  static const int ok = 1;
  static const int error = 0;
}

class ResourceConfig {
  ResourceConfig._();
  static const String loadingGifDefault = 'assets/gif/loading_medium.gif';
}

class Endpoints {
  Endpoints._();

  static const bool debug = false;

  static const String encuestaServiceUrl =
      debug ? 'http://192.168.18.4:5001' : 'http://192.168.18.7:44325';

  // receiveTimeout
  static const int receiveTimeout = 10000;

  // connectTimeout
  static const int connectionTimeout = 30000;

  //======================================[Encuesta Service]=======================================//

  //====Usuario====//

  // Login
  // SI SE USA
  // https://localhost:44325/api/Usuario/login?nomUsuario=Admin&claveUsuario=Admin
  static const String getLogin = '/api/Usuario/login';

  //====Encuesta====//

  // ObtenerEncuesta
  // SI SE USA
  // https://localhost:44325/api/Encuesta/obtenerEncuesta?codUsuario=1
  static const String getSurvey = '/api/Encuesta/obtenerEncuesta';

  // ObtenerReporte
  // SI SE USA
  // https://localhost:44325/api/Encuesta/obtenerReporte?codEncuesta=1
  static const String getReport = '/api/Encuesta/obtenerReporte';

  // RegistrarVotacion
  // SI SE USA
  // https://localhost:44325/api/Encuesta/registrarVotacion
  static const String postRegisterVote = '/api/Encuesta/registrarVotacion';
}
