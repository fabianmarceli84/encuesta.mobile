import 'dart:developer';
import 'package:app_mobile_encuesta/data/constants/data_constants.dart';
import 'package:app_mobile_encuesta/data/network/apis/encuesta/dto/survey/register_vote_request.dart';
import 'package:app_mobile_encuesta/data/network/apis/encuesta/dto/survey/register_vote_response.dart';
import 'package:app_mobile_encuesta/data/network/apis/encuesta/dto/survey/report_response.dart';
import 'package:app_mobile_encuesta/data/network/apis/encuesta/dto/survey/survey_response.dart';
import 'package:app_mobile_encuesta/data/network/apis/encuesta/dto/user/login_response.dart';
import 'package:app_mobile_encuesta/data/network/dio_client.dart';

class EncuestaApi {
  // dio instance
  final DioClient _dioClient;

  // injecting dio instance
  EncuestaApi(this._dioClient);

  //====Usuario====//

  ///Encuesta/api/Usuario/login
  ///
  ///@username user.
  ///
  ///@password user password.
  Future<LoginResponse> getLogin(
    String username,
    String password,
  ) async {
    try {
      final Map<String, dynamic> params = {
        'nomUsuario': username,
        'claveUsuario': password,
      };

      final res = await _dioClient.get(
        Endpoints.getLogin,
        queryParameters: params,
      );
      return LoginResponse.fromMap(res);
    } catch (e) {
      log(e.toString());
      rethrow;
    }
  }

  //====Encuesta====//

  ///Encuesta/api/Encuesta/obtenerEncuesta
  ///
  ///@userCode
  Future<SurveyResponse> getSurvey(
    int userCode,
  ) async {
    try {
      final Map<String, dynamic> params = {
        'codUsuario': userCode,
      };
      final res = await _dioClient.get(
        Endpoints.getSurvey,
        queryParameters: params,
      );
      return SurveyResponse.fromMap(res);
    } catch (e) {
      log(e.toString());
      rethrow;
    }
  }

  ///Encuesta/api/Encuesta/obtenerReporte
  ///
  ///@surveyCode
  Future<ReportResponse> getReport(
    int surveyCode,
  ) async {
    try {
      final Map<String, dynamic> params = {
        'codEncuesta': surveyCode,
      };
      final res = await _dioClient.get(
        Endpoints.getReport,
        queryParameters: params,
      );
      return ReportResponse.fromMap(res);
    } catch (e) {
      log(e.toString());
      rethrow;
    }
  }

  ///Encuesta/api/Encuesta/registrarVotacion
  Future<RegisterVoteResponse> postRegisterVote(
    RegisterVoteRequest request,
  ) async {
    try {
      final body = request.toJson();
      final res = await _dioClient.post(
        Endpoints.postRegisterVote,
        data: body,
      );
      return RegisterVoteResponse.fromMap(res);
    } catch (e) {
      log(e.toString());
      rethrow;
    }
  }
}
