// To parse this JSON data, do
//

import 'dart:convert';
import 'package:app_mobile_encuesta/data/network/apis/response.dart';

class LoginResponse implements OperationResult<Login> {
  LoginResponse({
    this.isValid = false,
    this.content,
    this.exceptions,
  });

  @override
  bool isValid;

  @override
  Login? content;

  @override
  List<ExceptionElement>? exceptions;

  factory LoginResponse.fromJson(String str) =>
      LoginResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory LoginResponse.fromMap(Map<String, dynamic> json) => LoginResponse(
        isValid: json['isValid'],
        content:
            json['content'] == null ? null : Login.fromMap(json['content']),
        exceptions: json['exceptions'] == null
            ? null
            : List<ExceptionElement>.from(
                json['exceptions'].map((x) => ExceptionElement.fromMap(x)),
              ),
      );

  Map<String, dynamic> toMap() => {
        'isValid': isValid,
        'content': content == null ? null : content!.toMap(),
        'exceptions': exceptions == null
            ? null
            : List<dynamic>.from(
                exceptions!.map((x) => x.toMap()),
              ),
      };
}

class Login {
  Login({
    this.codUsuario,
    this.codUsuarioTipo,
  });

  final int? codUsuario;
  final int? codUsuarioTipo;

  factory Login.fromJson(String str) => Login.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Login.fromMap(Map<String, dynamic> json) => Login(
        codUsuario: json['codUsuario'],
        codUsuarioTipo: json['codUsuarioTipo'],
      );

  Map<String, dynamic> toMap() => {
        'codUsuario': codUsuario,
        'codUsuarioTipo': codUsuarioTipo,
      };
}
