// To parse this JSON data, do
//

import 'dart:convert';
import 'package:app_mobile_encuesta/data/network/apis/response.dart';

class ReportResponse implements OperationResult<Report> {
  ReportResponse({
    this.isValid = false,
    this.content,
    this.exceptions,
  });

  @override
  bool isValid;

  @override
  Report? content;

  @override
  List<ExceptionElement>? exceptions;

  factory ReportResponse.fromJson(String str) =>
      ReportResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory ReportResponse.fromMap(Map<String, dynamic> json) => ReportResponse(
        isValid: json['isValid'],
        content:
            json['content'] == null ? null : Report.fromMap(json['content']),
        exceptions: json['exceptions'] == null
            ? null
            : List<ExceptionElement>.from(
                json['exceptions'].map((x) => ExceptionElement.fromMap(x)),
              ),
      );

  Map<String, dynamic> toMap() => {
        'isValid': isValid,
        'content': content == null ? null : content!.toMap(),
        'exceptions': exceptions == null
            ? null
            : List<dynamic>.from(
                exceptions!.map((x) => x.toMap()),
              ),
      };
}

class Report {
  Report({
    this.codEncuestaDetalle,
    this.nomEncuesta,
    this.pregunta,
    this.numRespuestaEncuesta,
    this.cantidadDetractores,
    this.cantidadNeutros,
    this.cantidadPromotores,
    this.nps,
  });

  final int? codEncuestaDetalle;
  final String? nomEncuesta;
  final String? pregunta;
  final int? numRespuestaEncuesta;
  final int? cantidadDetractores;
  final int? cantidadNeutros;
  final int? cantidadPromotores;
  final dynamic nps;

  factory Report.fromJson(String str) => Report.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Report.fromMap(Map<String, dynamic> json) => Report(
        codEncuestaDetalle: json['codEncuestaDetalle'],
        nomEncuesta: json['nomEncuesta'],
        pregunta: json['pregunta'],
        numRespuestaEncuesta: json['numRespuestaEncuesta'],
        cantidadDetractores: json['cantidadDetractores'],
        cantidadNeutros: json['cantidadNeutros'],
        cantidadPromotores: json['cantidadPromotores'],
        nps: json['nps'],
      );

  Map<String, dynamic> toMap() => {
        'codEncuestaDetalle': codEncuestaDetalle,
        'nomEncuesta': nomEncuesta,
        'pregunta': pregunta,
        'numRespuestaEncuesta': numRespuestaEncuesta,
        'cantidadDetractores': cantidadDetractores,
        'cantidadNeutros': cantidadNeutros,
        'cantidadPromotores': cantidadPromotores,
        'nps': nps,
      };
}
