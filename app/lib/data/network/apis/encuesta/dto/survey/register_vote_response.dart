// To parse this JSON data, do
//

import 'dart:convert';
import 'package:app_mobile_encuesta/data/network/apis/response.dart';

class RegisterVoteResponse implements OperationResult<String> {
  RegisterVoteResponse({
    this.isValid = false,
    this.content,
    this.exceptions,
  });

  @override
  bool isValid;

  @override
  String? content;

  @override
  List<ExceptionElement>? exceptions;

  factory RegisterVoteResponse.fromJson(String str) =>
      RegisterVoteResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory RegisterVoteResponse.fromMap(Map<String, dynamic> json) =>
      RegisterVoteResponse(
        isValid: json['isValid'],
        content: json['content'] == null ? null : json['content']!,
        exceptions: json['exceptions'] == null
            ? null
            : List<ExceptionElement>.from(
                json['exceptions'].map((x) => ExceptionElement.fromMap(x)),
              ),
      );

  Map<String, dynamic> toMap() => {
        'isValid': isValid,
        'content': content == null ? null : content!,
        'exceptions': exceptions == null
            ? null
            : List<dynamic>.from(
                exceptions!.map((x) => x.toMap()),
              ),
      };
}
