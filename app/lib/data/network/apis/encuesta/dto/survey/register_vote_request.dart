// To parse this JSON data, do
//

import 'dart:convert';

class RegisterVoteRequest {
  RegisterVoteRequest({
    required this.codUsuario,
    required this.codEncuestaDetalle,
    required this.respuestaPregunta,
  });

  final int codUsuario;
  final int codEncuestaDetalle;
  final String respuestaPregunta;

  factory RegisterVoteRequest.fromJson(String str) =>
      RegisterVoteRequest.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory RegisterVoteRequest.fromMap(Map<String, dynamic> json) =>
      RegisterVoteRequest(
        codUsuario: json["codUsuario"],
        codEncuestaDetalle: json["codEncuestaDetalle"],
        respuestaPregunta: json["respuestaPregunta"],
      );

  Map<String, dynamic> toMap() => {
        "codUsuario": codUsuario,
        "codEncuestaDetalle": codEncuestaDetalle,
        "respuestaPregunta": respuestaPregunta,
      };
}
