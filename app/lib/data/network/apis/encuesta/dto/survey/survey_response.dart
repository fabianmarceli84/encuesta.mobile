// To parse this JSON data, do
//

import 'dart:convert';
import 'package:app_mobile_encuesta/data/network/apis/response.dart';

class SurveyResponse implements OperationResult<Survey> {
  SurveyResponse({
    this.isValid = false,
    this.content,
    this.exceptions,
  });

  @override
  bool isValid;

  @override
  Survey? content;

  @override
  List<ExceptionElement>? exceptions;

  factory SurveyResponse.fromJson(String str) =>
      SurveyResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory SurveyResponse.fromMap(Map<String, dynamic> json) => SurveyResponse(
        isValid: json['isValid'],
        content:
            json['content'] == null ? null : Survey.fromMap(json['content']),
        exceptions: json['exceptions'] == null
            ? null
            : List<ExceptionElement>.from(
                json['exceptions'].map((x) => ExceptionElement.fromMap(x)),
              ),
      );

  Map<String, dynamic> toMap() => {
        'isValid': isValid,
        'content': content == null ? null : content!.toMap(),
        'exceptions': exceptions == null
            ? null
            : List<dynamic>.from(
                exceptions!.map((x) => x.toMap()),
              ),
      };
}

class Survey {
  Survey({
    this.codEncuestaDetalle,
    this.pregunta,
    this.respondioCliente,
  });

  final int? codEncuestaDetalle;
  final String? pregunta;
  final int? respondioCliente;

  factory Survey.fromJson(String str) => Survey.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Survey.fromMap(Map<String, dynamic> json) => Survey(
        codEncuestaDetalle: json['codEncuestaDetalle'],
        pregunta: json['pregunta'],
        respondioCliente: json['respondioCliente'],
      );

  Map<String, dynamic> toMap() => {
        'codEncuestaDetalle': codEncuestaDetalle,
        'pregunta': pregunta,
        'respondioCliente': respondioCliente,
      };
}
