import 'dart:convert';
import 'package:app_mobile_encuesta/data/constants/data_constants.dart';

class OperationResult<T> {
  bool isValid = false;
  List<ExceptionElement>? exceptions;
  T? content;
}

class ExceptionElement {
  ExceptionElement({
    this.code,
    this.description,
  });

  final String? code;
  final String? description;

  factory ExceptionElement.fromJson(String str) =>
      ExceptionElement.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory ExceptionElement.fromMap(Map<String, dynamic> json) =>
      ExceptionElement(
        code: json["code"],
        description: json["description"],
      );

  Map<String, dynamic> toMap() => {
        "code": code,
        "description": description,
      };
}

class UIResponse<T> {
  bool isValid = false;
  String errorMessage = GenericErrorMessages.logic;
  String successfulMessage = '';
  T? content;
}
