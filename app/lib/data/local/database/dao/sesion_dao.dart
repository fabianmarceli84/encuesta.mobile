// the _TodosDaoMixin will be created by drift. It contains all the necessary
// fields for the tables. The <MyDatabase> type annotation is the database class
// that should use this dao.
import 'package:app_mobile_encuesta/data/local/database/db.dart';
import 'package:app_mobile_encuesta/data/local/database/entities/sesion.dart';
import 'package:drift/drift.dart';

part 'sesion_dao.drift.dart';

@DriftAccessor(tables: [Sesion])
class SesionDao extends DatabaseAccessor<EncuestaDatabase>
    with _$SesionDaoMixin {
  // this constructor is required so that the main database can create an instance
  // of this object.
  SesionDao(EncuestaDatabase db) : super(db);

  Future<int> insert(SesionCompanion data) {
    return sesion.insertOne(data);
  }

  Future<SesionData?> getLast() {
    final sesionData = (sesion.select()
          ..orderBy(
              [(s) => OrderingTerm(expression: s.id, mode: OrderingMode.desc)])
          ..limit(1))
        .getSingleOrNull();
    return sesionData;
  }

  Stream<SesionData> streamGetLast() {
    final sesionData = (sesion.select()
          ..orderBy(
              [(s) => OrderingTerm(expression: s.id, mode: OrderingMode.desc)])
          ..limit(1))
        .watchSingle();
    return sesionData;
  }
}
