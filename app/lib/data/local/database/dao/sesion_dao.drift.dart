// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sesion_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$SesionDaoMixin on DatabaseAccessor<EncuestaDatabase> {
  $SesionTable get sesion => attachedDatabase.sesion;
}
