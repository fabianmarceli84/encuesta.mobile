// ignore_for_file: unused_element

import 'dart:developer';
import 'dart:io';
import 'package:app_mobile_encuesta/data/constants/data_constants.dart';
import 'package:app_mobile_encuesta/data/local/database/dao/sesion_dao.dart';
import 'package:app_mobile_encuesta/data/local/database/entities/sesion.dart';
import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;

part 'db.drift.dart';

@DriftDatabase(tables: [
  Sesion,
], daos: [
  SesionDao,
])
class EncuestaDatabase extends _$EncuestaDatabase {
  // we tell the database where to store the data with this constructor
  EncuestaDatabase() : super(NativeDatabase.memory());

  // this is the new constructor for insolate :D
  EncuestaDatabase.connect(DatabaseConnection connection)
      : super.connect(connection);

  // you should bump this number whenever you change or add a table definition.
  // Migrations are covered later in the documentation.
  @override
  int get schemaVersion => 1;

  @override
  MigrationStrategy get migration => MigrationStrategy(onCreate: (Migrator m) {
        return m.createAll();
      }, onUpgrade: (Migrator m, int from, int to) async {
        log("Drift migration: from: $from to: $to");
        if (from == 1) {
          // await m.addColumn(sesion, sesion.codUsuario); //example
        }
      });

  Future<void> exportInto(File file) async {
    // Make sure the directory of the target file exists
    await file.parent.create(recursive: true);

    // Override an existing backup, sqlite expects the target file to be empty
    if (file.existsSync()) {
      file.deleteSync();
    }

    await customStatement('VACUUM INTO ?', [file.path]);
  }
}

//Works on the main thread of the app, causing graphics slowdown
LazyDatabase _openConnection() {
  // the LazyDatabase util lets us find the right location for the file async.
  return LazyDatabase(() async {
    // put the database file, called db.sqlite here, into the documents folder
    // for your app.
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, DBConstants.name));
    return NativeDatabase(file);
  });
}
