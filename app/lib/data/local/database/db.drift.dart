// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'db.dart';

// **************************************************************************
// DriftDatabaseGenerator
// **************************************************************************

// ignore_for_file: type=lint
class SesionData extends DataClass implements Insertable<SesionData> {
  final int id;
  final int codUsuario;
  final int codUsuarioTipo;
  final String nombreUsuario;
  final String clave;
  const SesionData(
      {required this.id,
      required this.codUsuario,
      required this.codUsuarioTipo,
      required this.nombreUsuario,
      required this.clave});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['cod_usuario'] = Variable<int>(codUsuario);
    map['cod_usuario_tipo'] = Variable<int>(codUsuarioTipo);
    map['nombre_usuario'] = Variable<String>(nombreUsuario);
    map['clave'] = Variable<String>(clave);
    return map;
  }

  SesionCompanion toCompanion(bool nullToAbsent) {
    return SesionCompanion(
      id: Value(id),
      codUsuario: Value(codUsuario),
      codUsuarioTipo: Value(codUsuarioTipo),
      nombreUsuario: Value(nombreUsuario),
      clave: Value(clave),
    );
  }

  factory SesionData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return SesionData(
      id: serializer.fromJson<int>(json['id']),
      codUsuario: serializer.fromJson<int>(json['codUsuario']),
      codUsuarioTipo: serializer.fromJson<int>(json['codUsuarioTipo']),
      nombreUsuario: serializer.fromJson<String>(json['nombreUsuario']),
      clave: serializer.fromJson<String>(json['clave']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'codUsuario': serializer.toJson<int>(codUsuario),
      'codUsuarioTipo': serializer.toJson<int>(codUsuarioTipo),
      'nombreUsuario': serializer.toJson<String>(nombreUsuario),
      'clave': serializer.toJson<String>(clave),
    };
  }

  SesionData copyWith(
          {int? id,
          int? codUsuario,
          int? codUsuarioTipo,
          String? nombreUsuario,
          String? clave}) =>
      SesionData(
        id: id ?? this.id,
        codUsuario: codUsuario ?? this.codUsuario,
        codUsuarioTipo: codUsuarioTipo ?? this.codUsuarioTipo,
        nombreUsuario: nombreUsuario ?? this.nombreUsuario,
        clave: clave ?? this.clave,
      );
  @override
  String toString() {
    return (StringBuffer('SesionData(')
          ..write('id: $id, ')
          ..write('codUsuario: $codUsuario, ')
          ..write('codUsuarioTipo: $codUsuarioTipo, ')
          ..write('nombreUsuario: $nombreUsuario, ')
          ..write('clave: $clave')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      Object.hash(id, codUsuario, codUsuarioTipo, nombreUsuario, clave);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is SesionData &&
          other.id == this.id &&
          other.codUsuario == this.codUsuario &&
          other.codUsuarioTipo == this.codUsuarioTipo &&
          other.nombreUsuario == this.nombreUsuario &&
          other.clave == this.clave);
}

class SesionCompanion extends UpdateCompanion<SesionData> {
  final Value<int> id;
  final Value<int> codUsuario;
  final Value<int> codUsuarioTipo;
  final Value<String> nombreUsuario;
  final Value<String> clave;
  const SesionCompanion({
    this.id = const Value.absent(),
    this.codUsuario = const Value.absent(),
    this.codUsuarioTipo = const Value.absent(),
    this.nombreUsuario = const Value.absent(),
    this.clave = const Value.absent(),
  });
  SesionCompanion.insert({
    this.id = const Value.absent(),
    required int codUsuario,
    required int codUsuarioTipo,
    required String nombreUsuario,
    required String clave,
  })  : codUsuario = Value(codUsuario),
        codUsuarioTipo = Value(codUsuarioTipo),
        nombreUsuario = Value(nombreUsuario),
        clave = Value(clave);
  static Insertable<SesionData> custom({
    Expression<int>? id,
    Expression<int>? codUsuario,
    Expression<int>? codUsuarioTipo,
    Expression<String>? nombreUsuario,
    Expression<String>? clave,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (codUsuario != null) 'cod_usuario': codUsuario,
      if (codUsuarioTipo != null) 'cod_usuario_tipo': codUsuarioTipo,
      if (nombreUsuario != null) 'nombre_usuario': nombreUsuario,
      if (clave != null) 'clave': clave,
    });
  }

  SesionCompanion copyWith(
      {Value<int>? id,
      Value<int>? codUsuario,
      Value<int>? codUsuarioTipo,
      Value<String>? nombreUsuario,
      Value<String>? clave}) {
    return SesionCompanion(
      id: id ?? this.id,
      codUsuario: codUsuario ?? this.codUsuario,
      codUsuarioTipo: codUsuarioTipo ?? this.codUsuarioTipo,
      nombreUsuario: nombreUsuario ?? this.nombreUsuario,
      clave: clave ?? this.clave,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (codUsuario.present) {
      map['cod_usuario'] = Variable<int>(codUsuario.value);
    }
    if (codUsuarioTipo.present) {
      map['cod_usuario_tipo'] = Variable<int>(codUsuarioTipo.value);
    }
    if (nombreUsuario.present) {
      map['nombre_usuario'] = Variable<String>(nombreUsuario.value);
    }
    if (clave.present) {
      map['clave'] = Variable<String>(clave.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('SesionCompanion(')
          ..write('id: $id, ')
          ..write('codUsuario: $codUsuario, ')
          ..write('codUsuarioTipo: $codUsuarioTipo, ')
          ..write('nombreUsuario: $nombreUsuario, ')
          ..write('clave: $clave')
          ..write(')'))
        .toString();
  }
}

class $SesionTable extends Sesion with TableInfo<$SesionTable, SesionData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $SesionTable(this.attachedDatabase, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints: 'PRIMARY KEY AUTOINCREMENT');
  final VerificationMeta _codUsuarioMeta = const VerificationMeta('codUsuario');
  @override
  late final GeneratedColumn<int> codUsuario = GeneratedColumn<int>(
      'cod_usuario', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  final VerificationMeta _codUsuarioTipoMeta =
      const VerificationMeta('codUsuarioTipo');
  @override
  late final GeneratedColumn<int> codUsuarioTipo = GeneratedColumn<int>(
      'cod_usuario_tipo', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  final VerificationMeta _nombreUsuarioMeta =
      const VerificationMeta('nombreUsuario');
  @override
  late final GeneratedColumn<String> nombreUsuario = GeneratedColumn<String>(
      'nombre_usuario', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  final VerificationMeta _claveMeta = const VerificationMeta('clave');
  @override
  late final GeneratedColumn<String> clave = GeneratedColumn<String>(
      'clave', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns =>
      [id, codUsuario, codUsuarioTipo, nombreUsuario, clave];
  @override
  String get aliasedName => _alias ?? 'sesion';
  @override
  String get actualTableName => 'sesion';
  @override
  VerificationContext validateIntegrity(Insertable<SesionData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('cod_usuario')) {
      context.handle(
          _codUsuarioMeta,
          codUsuario.isAcceptableOrUnknown(
              data['cod_usuario']!, _codUsuarioMeta));
    } else if (isInserting) {
      context.missing(_codUsuarioMeta);
    }
    if (data.containsKey('cod_usuario_tipo')) {
      context.handle(
          _codUsuarioTipoMeta,
          codUsuarioTipo.isAcceptableOrUnknown(
              data['cod_usuario_tipo']!, _codUsuarioTipoMeta));
    } else if (isInserting) {
      context.missing(_codUsuarioTipoMeta);
    }
    if (data.containsKey('nombre_usuario')) {
      context.handle(
          _nombreUsuarioMeta,
          nombreUsuario.isAcceptableOrUnknown(
              data['nombre_usuario']!, _nombreUsuarioMeta));
    } else if (isInserting) {
      context.missing(_nombreUsuarioMeta);
    }
    if (data.containsKey('clave')) {
      context.handle(
          _claveMeta, clave.isAcceptableOrUnknown(data['clave']!, _claveMeta));
    } else if (isInserting) {
      context.missing(_claveMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  SesionData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return SesionData(
      id: attachedDatabase.options.types
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      codUsuario: attachedDatabase.options.types
          .read(DriftSqlType.int, data['${effectivePrefix}cod_usuario'])!,
      codUsuarioTipo: attachedDatabase.options.types
          .read(DriftSqlType.int, data['${effectivePrefix}cod_usuario_tipo'])!,
      nombreUsuario: attachedDatabase.options.types
          .read(DriftSqlType.string, data['${effectivePrefix}nombre_usuario'])!,
      clave: attachedDatabase.options.types
          .read(DriftSqlType.string, data['${effectivePrefix}clave'])!,
    );
  }

  @override
  $SesionTable createAlias(String alias) {
    return $SesionTable(attachedDatabase, alias);
  }
}

abstract class _$EncuestaDatabase extends GeneratedDatabase {
  _$EncuestaDatabase(QueryExecutor e) : super(e);
  _$EncuestaDatabase.connect(DatabaseConnection c) : super.connect(c);
  late final $SesionTable sesion = $SesionTable(this);
  late final SesionDao sesionDao = SesionDao(this as EncuestaDatabase);
  @override
  Iterable<TableInfo<Table, dynamic>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [sesion];
  @override
  DriftDatabaseOptions get options =>
      const DriftDatabaseOptions(storeDateTimeAsText: true);
}
