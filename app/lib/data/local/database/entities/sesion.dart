import 'package:drift/drift.dart';

class Sesion extends Table {
  IntColumn get id => integer().autoIncrement()();
  IntColumn get codUsuario => integer()();
  IntColumn get codUsuarioTipo => integer()();
  TextColumn get nombreUsuario => text()();
  TextColumn get clave => text()();
}
