import 'dart:async';
import 'package:app_mobile_encuesta/data/constants/data_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceHelper {
  // shared pref instance
  final SharedPreferences _sharedPreference;

  // constructor
  SharedPreferenceHelper(this._sharedPreference);

  // General Methods: ----------------------------------------------------------
  String? get uuidMobile {
    return _sharedPreference.getString(Preferences.uuid_mobile);
  }

  Future<bool> saveUUIDMobile(String uuid) async {
    return _sharedPreference.setString(Preferences.uuid_mobile, uuid);
  }

  Future<bool> removeUUIDMobile() async {
    return _sharedPreference.remove(Preferences.uuid_mobile);
  }

  // Login:---------------------------------------------------------------------
  get isLoggedIn {
    return _sharedPreference.getBool(Preferences.is_logged_in) ?? false;
  }

  Future<bool> saveIsLoggedIn(bool value) async {
    return _sharedPreference.setBool(Preferences.is_logged_in, value);
  }

  bool get isNewLogin {
    return _sharedPreference.getBool(Preferences.is_new_login) ?? true;
  }

  Future<void> changeNewLogin(bool value) {
    return _sharedPreference.setBool(Preferences.is_new_login, value);
  }

  // Biometric auth:---------------------------------------------------------------
  bool get isFingerprintAuth {
    return _sharedPreference.getBool(Preferences.is_biometric) ?? false;
  }

  Future<void> changeFingerprintAuth(bool value) {
    return _sharedPreference.setBool(Preferences.is_biometric, value);
  }

  bool get isAutomaticLogin {
    return _sharedPreference.getBool(Preferences.is_automatic_login) ?? false;
  }

  Future<void> changeAutomaticLogin(bool value) {
    return _sharedPreference.setBool(Preferences.is_automatic_login, value);
  }

  // Theme:---------------------------------------------------------------------
  bool get isDarkMode {
    return _sharedPreference.getBool(Preferences.is_dark_mode) ?? false;
  }

  Future<void> changeBrightnessToDark(bool value) {
    return _sharedPreference.setBool(Preferences.is_dark_mode, value);
  }

  bool get isFullScreenMode {
    return _sharedPreference.getBool(Preferences.is_fullscreen_mode) ?? false;
  }

  Future<void> changeFullScreenMode(bool value) {
    return _sharedPreference.setBool(Preferences.is_fullscreen_mode, value);
  }

  int get primaryColor {
    return _sharedPreference.getInt(Preferences.primary_color) ?? 0xFFA20101;
  }

  Future<void> changePrimaryColor(int value) {
    return _sharedPreference.setInt(Preferences.primary_color, value);
  }
}
