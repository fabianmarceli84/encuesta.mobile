import 'dart:async';
import 'dart:developer';
import 'dart:io';
import 'package:app_mobile_encuesta/di/components/service_locator.dart';
import 'package:app_mobile_encuesta/ui/app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  ///Certificado para equipos antiguos
  // ByteData data = await PlatformAssetBundle()
  //     .load('assets/certificates/lets-encrypt-r3.pem');
  // SecurityContext.defaultContext
  //     .setTrustedCertificatesBytes(data.buffer.asUint8List());

  await setPreferredOrientations();
  await setupLocator();
  return runZonedGuarded(() async {
    runApp(const App());
  }, (error, stack) {
    //Only app log => I/flutter, [log]
    log('$stack');
    log('$error');
  });
}

Future<void> setPreferredOrientations() {
  return SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);
}

Future<void> setEnabledSystemUIMode() {
  return SystemChrome.setEnabledSystemUIMode(
    SystemUiMode.immersiveSticky,
    overlays: [SystemUiOverlay.top],
  );
}
