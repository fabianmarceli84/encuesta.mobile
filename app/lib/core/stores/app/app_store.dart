// ignore_for_file: unnecessary_this, non_constant_identifier_names, unused_field, library_private_types_in_public_api

import 'dart:io';
import 'package:app_mobile_encuesta/core/model/session.dart';
import 'package:app_mobile_encuesta/core/stores/error/error_store.dart';
import 'package:app_mobile_encuesta/data/repository.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/services.dart';
import 'package:mac_address/mac_address.dart';
import 'package:mobx/mobx.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:uuid/uuid.dart';

part 'app_store.g.dart';

class AppStore = _AppStore with _$AppStore;

abstract class _AppStore with Store {
  final String TAG = "_AppStore";

  // repository instance
  final Repository _repository;

  // store for handling error messages
  final ErrorStore errorStore = ErrorStore();

  // constructor:---------------------------------------------------------------
  _AppStore(Repository repository) : this._repository = repository {
    _setupValidations();
    _checkUUID();
    _init();
  }

  // disposers:-----------------------------------------------------------------
  late List<ReactionDisposer> _disposers;

  void _setupValidations() {
    _disposers = [];
  }

  // empty values

  // store variables:-----------------------------------------------------------
  @observable
  bool loading = false;

  @observable
  bool success = false;

  @observable
  bool isNewLogin = true;

  @observable
  bool isLoggedIn = false;

  @observable
  String uuid = '';

  @observable
  Session? session;

  @observable
  PackageInfo packageInfo = PackageInfo(
    appName: 'Encuesta',
    packageName: 'com.fabian.encuesta',
    version: '',
    buildNumber: '',
    buildSignature: '',
  );

  @observable
  String deviceModel = '';

  @observable
  AndroidDeviceInfo? androidInfo;

  @observable
  String sesionToken = '';

  // computed:------------------------------------------------------------------
  @computed
  Future<Session?> get currentSession async {
    return _loadSession();
  }

  @computed
  Future<PackageInfo> get currentPackageInfo async {
    return _loadPackageInfo();
  }

  // actions:-------------------------------------------------------------------
  @action
  Future changeNewLogin(bool value) async {
    loading = true;

    isNewLogin = value;
    await _repository.changeNewLogin(value);

    loading = false;
  }

  @action
  Future changeLoggedIn(bool value) async {
    loading = true;

    isLoggedIn = value;
    await _repository.saveIsLoggedIn(value);

    loading = false;
  }

  @action
  Future logout() async {
    await _repository.changeNewLogin(true);
    await _repository.saveIsLoggedIn(false);
  }

  @action
  Future reload() async {
    await _init();
  }

  // general methods:-----------------------------------------------------------
  Future<Session?> _loadSession() async {
    if (!_repository.isLoggedIn) return null;

    final data = await _repository.getLastSesion();
    if (data == null) return null;

    final session = Session(
      userCode: data.codUsuario,
      userCodeType: data.codUsuarioTipo,
      userName: data.nombreUsuario,
      password: data.clave,
    );

    return session;
  }

  Future _checkUUID() async {
    final String uuidGenerated;

    final uuidDB = _repository.uuidMobile;
    if (uuidDB == null) {
      String macAddress;

      try {
        macAddress = await GetMac.macAddress;
      } on PlatformException {
        macAddress = '';
      }

      if (macAddress.isNotEmpty) {
        uuidGenerated = macAddress;
      } else {
        uuidGenerated = const Uuid().v4();
      }

      await _repository.saveUUIDMobile(uuidGenerated);
    } else {
      uuidGenerated = uuidDB;
    }
    uuid = uuidGenerated;
  }

  Future<PackageInfo> _loadPackageInfo() async {
    return await PackageInfo.fromPlatform();
  }

  Future<String> _loadDeviceModel() async {
    final deviceInfoPlugin = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      return (await deviceInfoPlugin.androidInfo).model ?? '';
    } else if (Platform.isIOS) {
      return (await deviceInfoPlugin.iosInfo).model ?? '';
    } else {
      return '';
    }
  }

  Future<AndroidDeviceInfo> _loadAndroidInfo() async {
    return await DeviceInfoPlugin().androidInfo;
  }

  Future _init() async {
    isNewLogin = _repository.isNewLogin;
    isLoggedIn = _repository.isLoggedIn;
    packageInfo = await _loadPackageInfo();
    deviceModel = await _loadDeviceModel();
    session = await _loadSession();
    androidInfo = await _loadAndroidInfo();
  }

  // dispose:-------------------------------------------------------------------
  dispose() {}
}
