// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$AppStore on _AppStore, Store {
  Computed<Future<Session?>>? _$currentSessionComputed;

  @override
  Future<Session?> get currentSession => (_$currentSessionComputed ??=
          Computed<Future<Session?>>(() => super.currentSession,
              name: '_AppStore.currentSession'))
      .value;
  Computed<Future<PackageInfo>>? _$currentPackageInfoComputed;

  @override
  Future<PackageInfo> get currentPackageInfo =>
      (_$currentPackageInfoComputed ??= Computed<Future<PackageInfo>>(
              () => super.currentPackageInfo,
              name: '_AppStore.currentPackageInfo'))
          .value;

  late final _$loadingAtom = Atom(name: '_AppStore.loading', context: context);

  @override
  bool get loading {
    _$loadingAtom.reportRead();
    return super.loading;
  }

  @override
  set loading(bool value) {
    _$loadingAtom.reportWrite(value, super.loading, () {
      super.loading = value;
    });
  }

  late final _$successAtom = Atom(name: '_AppStore.success', context: context);

  @override
  bool get success {
    _$successAtom.reportRead();
    return super.success;
  }

  @override
  set success(bool value) {
    _$successAtom.reportWrite(value, super.success, () {
      super.success = value;
    });
  }

  late final _$isNewLoginAtom =
      Atom(name: '_AppStore.isNewLogin', context: context);

  @override
  bool get isNewLogin {
    _$isNewLoginAtom.reportRead();
    return super.isNewLogin;
  }

  @override
  set isNewLogin(bool value) {
    _$isNewLoginAtom.reportWrite(value, super.isNewLogin, () {
      super.isNewLogin = value;
    });
  }

  late final _$isLoggedInAtom =
      Atom(name: '_AppStore.isLoggedIn', context: context);

  @override
  bool get isLoggedIn {
    _$isLoggedInAtom.reportRead();
    return super.isLoggedIn;
  }

  @override
  set isLoggedIn(bool value) {
    _$isLoggedInAtom.reportWrite(value, super.isLoggedIn, () {
      super.isLoggedIn = value;
    });
  }

  late final _$uuidAtom = Atom(name: '_AppStore.uuid', context: context);

  @override
  String get uuid {
    _$uuidAtom.reportRead();
    return super.uuid;
  }

  @override
  set uuid(String value) {
    _$uuidAtom.reportWrite(value, super.uuid, () {
      super.uuid = value;
    });
  }

  late final _$sessionAtom = Atom(name: '_AppStore.session', context: context);

  @override
  Session? get session {
    _$sessionAtom.reportRead();
    return super.session;
  }

  @override
  set session(Session? value) {
    _$sessionAtom.reportWrite(value, super.session, () {
      super.session = value;
    });
  }

  late final _$packageInfoAtom =
      Atom(name: '_AppStore.packageInfo', context: context);

  @override
  PackageInfo get packageInfo {
    _$packageInfoAtom.reportRead();
    return super.packageInfo;
  }

  @override
  set packageInfo(PackageInfo value) {
    _$packageInfoAtom.reportWrite(value, super.packageInfo, () {
      super.packageInfo = value;
    });
  }

  late final _$deviceModelAtom =
      Atom(name: '_AppStore.deviceModel', context: context);

  @override
  String get deviceModel {
    _$deviceModelAtom.reportRead();
    return super.deviceModel;
  }

  @override
  set deviceModel(String value) {
    _$deviceModelAtom.reportWrite(value, super.deviceModel, () {
      super.deviceModel = value;
    });
  }

  late final _$androidInfoAtom =
      Atom(name: '_AppStore.androidInfo', context: context);

  @override
  AndroidDeviceInfo? get androidInfo {
    _$androidInfoAtom.reportRead();
    return super.androidInfo;
  }

  @override
  set androidInfo(AndroidDeviceInfo? value) {
    _$androidInfoAtom.reportWrite(value, super.androidInfo, () {
      super.androidInfo = value;
    });
  }

  late final _$sesionTokenAtom =
      Atom(name: '_AppStore.sesionToken', context: context);

  @override
  String get sesionToken {
    _$sesionTokenAtom.reportRead();
    return super.sesionToken;
  }

  @override
  set sesionToken(String value) {
    _$sesionTokenAtom.reportWrite(value, super.sesionToken, () {
      super.sesionToken = value;
    });
  }

  late final _$changeNewLoginAsyncAction =
      AsyncAction('_AppStore.changeNewLogin', context: context);

  @override
  Future<dynamic> changeNewLogin(bool value) {
    return _$changeNewLoginAsyncAction.run(() => super.changeNewLogin(value));
  }

  late final _$changeLoggedInAsyncAction =
      AsyncAction('_AppStore.changeLoggedIn', context: context);

  @override
  Future<dynamic> changeLoggedIn(bool value) {
    return _$changeLoggedInAsyncAction.run(() => super.changeLoggedIn(value));
  }

  late final _$logoutAsyncAction =
      AsyncAction('_AppStore.logout', context: context);

  @override
  Future<dynamic> logout() {
    return _$logoutAsyncAction.run(() => super.logout());
  }

  late final _$reloadAsyncAction =
      AsyncAction('_AppStore.reload', context: context);

  @override
  Future<dynamic> reload() {
    return _$reloadAsyncAction.run(() => super.reload());
  }

  @override
  String toString() {
    return '''
loading: ${loading},
success: ${success},
isNewLogin: ${isNewLogin},
isLoggedIn: ${isLoggedIn},
uuid: ${uuid},
session: ${session},
packageInfo: ${packageInfo},
deviceModel: ${deviceModel},
androidInfo: ${androidInfo},
sesionToken: ${sesionToken},
currentSession: ${currentSession},
currentPackageInfo: ${currentPackageInfo}
    ''';
  }
}
