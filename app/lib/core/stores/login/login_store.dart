// ignore_for_file: unnecessary_this, non_constant_identifier_names, unused_field, library_private_types_in_public_api

import 'package:app_mobile_encuesta/core/model/session.dart';
import 'package:app_mobile_encuesta/core/stores/error/error_store.dart';
import 'package:app_mobile_encuesta/data/constants/data_constants.dart';
import 'package:app_mobile_encuesta/data/local/database/db.dart';
import 'package:app_mobile_encuesta/data/network/apis/encuesta/dto/user/login_response.dart';
import 'package:app_mobile_encuesta/data/repository.dart';
import 'package:app_mobile_encuesta/utils/utils.dart';
import 'package:mobx/mobx.dart';

part 'login_store.g.dart';

class LoginStore = _LoginStore with _$LoginStore;

abstract class _LoginStore with Store {
  final String TAG = "_LoginStore";

  // repository instance
  final Repository _repository;

  // store for handling form errors
  final FormErrorStore formErrorStore = FormErrorStore();

  // store for handling error messages
  final ErrorStore errorStore = ErrorStore();

  // constructor:---------------------------------------------------------------
  _LoginStore(Repository repository) : this._repository = repository {
    _setupValidations();
  }

  // disposers:-----------------------------------------------------------------
  late List<ReactionDisposer> _disposers;

  void _setupValidations() {
    _disposers = [
      reaction((_) => username, validateUsername),
      reaction((_) => password, validatePassword)
    ];
  }

  // store variables:-----------------------------------------------------------
  @observable
  String username = '';

  @observable
  String password = '';

  @observable
  bool obscure = true;

  @observable
  bool loading = false;

  @observable
  bool success = false;

  // computed:------------------------------------------------------------------
  @computed
  bool get canLogin =>
      !formErrorStore.hasErrorsInLogin &&
      username.isNotEmpty &&
      password.isNotEmpty;

  // actions:-------------------------------------------------------------------
  @action
  void setUsername(String value) {
    username = value;
  }

  @action
  void setPassword(String value) {
    password = value;
  }

  @action
  void setObscure(bool value) {
    obscure = value;
  }

  @action
  Future login() async {
    loading = true;

    final usernameFinal = username.trim();
    final passwordFinal = password.trim();

    final future = _repository.getLogin(
      usernameFinal,
      passwordFinal,
    );

    future.then((result) async {
      if (!result.isValid || result.content == null) {
        errorStore.errorMessage = Utils.getErrorMessage(result.exceptions) ??
            GenericErrorMessages.logic;
        success = false;
        return;
      }

      final resultSesionDB = await _saveUser(
        result,
        usernameFinal,
        passwordFinal,
      );

      if (resultSesionDB <= 0) {
        errorStore.errorMessage = GenericErrorMessages.database;
        success = false;
        return;
      }

      await _repository.saveIsLoggedIn(true);
      await _repository.changeAutomaticLogin(true);
      success = true;
    }).catchError((error) {
      errorStore.errorMessage = GenericErrorMessages.logic;
      success = false;
    }).whenComplete(() => loading = false);
  }

  @action
  Future loginAuth(
    Session sesion, {
    isBiometric = true,
  }) async {
    loading = true;

    final usernameFinal = isBiometric ? sesion.userName : username;
    final passwordFinal = isBiometric ? sesion.password : password;

    final future = _repository.getLogin(
      usernameFinal,
      passwordFinal,
    );

    future.then((result) async {
      if (!result.isValid || result.content == null) {
        errorStore.errorMessage = Utils.getErrorMessage(result.exceptions) ??
            GenericErrorMessages.logic;
        success = false;
        return;
      }

      final resultSesionDB = await _saveUser(
        result,
        usernameFinal,
        passwordFinal,
      );

      if (resultSesionDB <= 0) {
        errorStore.errorMessage = GenericErrorMessages.database;
        success = false;
        return;
      }

      success = true;
    }).catchError((error) {
      errorStore.errorMessage = GenericErrorMessages.logic;
      success = false;
    }).whenComplete(() => loading = false);
  }

  @action
  void validateUsername(String value) {
    if (value.isEmpty) {
      formErrorStore.username = "El usuario no puede estar vacío";
    } else {
      formErrorStore.username = null;
    }
  }

  @action
  void validatePassword(String value) {
    if (value.isEmpty) {
      formErrorStore.password = "La contraseña no puede estar vacío";
    } else {
      formErrorStore.password = null;
    }
  }

  // general methods:-----------------------------------------------------------
  void validateAll() {
    validateUsername(username);
    validatePassword(password);
  }

  Future<int> _saveUser(
    LoginResponse loginResponse,
    String username,
    String password,
  ) async {
    final data = SesionCompanion.insert(
      codUsuario: loginResponse.content!.codUsuario!,
      codUsuarioTipo: loginResponse.content!.codUsuarioTipo!,
      nombreUsuario: username,
      clave: password,
    );
    return await _repository.insertSesion(data);
  }

  // dispose:-------------------------------------------------------------------
  dispose() {}
}

class FormErrorStore = _FormErrorStore with _$FormErrorStore;

abstract class _FormErrorStore with Store {
  @observable
  String? username;

  @observable
  String? password;

  @computed
  bool get hasErrorsInLogin => username != null || password != null;

  @computed
  bool get hasErrorsInRegister => username != null || password != null;

  @computed
  bool get hasErrorInForgotPassword => username != null;
}
