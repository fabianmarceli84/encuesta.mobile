// ignore_for_file: unnecessary_this, non_constant_identifier_names, unused_field, depend_on_referenced_packages, library_private_types_in_public_api

import 'package:app_mobile_encuesta/core/stores/error/error_store.dart';
import 'package:app_mobile_encuesta/data/constants/data_constants.dart';
import 'package:app_mobile_encuesta/data/repository.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'package:mobx/mobx.dart';
import 'package:local_auth_android/local_auth_android.dart';
import 'package:local_auth_ios/local_auth_ios.dart';

part 'biometric_store.g.dart';

class BiometricStore = _BiometricStore with _$BiometricStore;

abstract class _BiometricStore with Store {
  final String TAG = "_BiometricStore";

  // repository instance
  final Repository _repository;

  // auth
  late LocalAuthentication _localAuth;

  // store for handling error messages
  final ErrorStore errorStore = ErrorStore();

  // constructor:---------------------------------------------------------------
  _BiometricStore(Repository repository) : this._repository = repository {
    _init();
    _setupValidations();
  }

  // disposers:-----------------------------------------------------------------
  late List<ReactionDisposer> _disposers;

  void _setupValidations() {
    _disposers = [];
  }

  // store variables:-----------------------------------------------------------
  @observable
  bool loading = false;

  @observable
  bool success = false;

  @observable
  bool biometricsAvailable = false;

  // computed:------------------------------------------------------------------
  @computed
  Future<bool> get canUseBiometrics async =>
      await _localAuth.canCheckBiometrics &&
      await _localAuth.isDeviceSupported();

  @computed
  Future<bool> get canUseFingerprint async {
    final check = await canUseBiometrics;
    if (!check) {
      return false;
    }
    final List<BiometricType> availableBiometrics =
        await _localAuth.getAvailableBiometrics();
    if (availableBiometrics.contains(BiometricType.strong) ||
        availableBiometrics.contains(BiometricType.fingerprint)) {
      return true;
    }
    return false;
  }

  // actions:-------------------------------------------------------------------
  @action
  Future reset() async {
    success = false;
  }

  @action
  Future checkBiometrics() async {
    biometricsAvailable = await canUseFingerprint;
  }

  @action
  Future<bool> authenticate() async {
    try {
      loading = true;
      final didAuthenticate = await _localAuth.authenticate(
          localizedReason: 'Por favor autentíquese para continuar',
          options: const AuthenticationOptions(biometricOnly: true),
          authMessages: const <AuthMessages>[
            AndroidAuthMessages(
              signInTitle: 'Lector de huella para Encuesta',
              biometricHint: 'Ingresa a la app Encuesta',
              cancelButton: 'CANCELAR',
            ),
            IOSAuthMessages(
              cancelButton: 'CANCELAR',
            ),
          ]);
      loading = false;
      if (didAuthenticate) {
        success = true;
        await _repository.changeAutomaticLogin(false);
      } else {
        success = false;
      }
      return didAuthenticate;
    } on PlatformException {
      loading = false;
      errorStore.errorMessage = GenericErrorMessages.logic;
      success = false;
      return false;
    }
  }

  // general:-------------------------------------------------------------------
  _init() {
    _localAuth = LocalAuthentication();
  }

  // dispose:-------------------------------------------------------------------
  dispose() {}
}
