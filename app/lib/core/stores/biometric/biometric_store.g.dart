// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'biometric_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$BiometricStore on _BiometricStore, Store {
  Computed<Future<bool>>? _$canUseBiometricsComputed;

  @override
  Future<bool> get canUseBiometrics => (_$canUseBiometricsComputed ??=
          Computed<Future<bool>>(() => super.canUseBiometrics,
              name: '_BiometricStore.canUseBiometrics'))
      .value;
  Computed<Future<bool>>? _$canUseFingerprintComputed;

  @override
  Future<bool> get canUseFingerprint => (_$canUseFingerprintComputed ??=
          Computed<Future<bool>>(() => super.canUseFingerprint,
              name: '_BiometricStore.canUseFingerprint'))
      .value;

  late final _$loadingAtom =
      Atom(name: '_BiometricStore.loading', context: context);

  @override
  bool get loading {
    _$loadingAtom.reportRead();
    return super.loading;
  }

  @override
  set loading(bool value) {
    _$loadingAtom.reportWrite(value, super.loading, () {
      super.loading = value;
    });
  }

  late final _$successAtom =
      Atom(name: '_BiometricStore.success', context: context);

  @override
  bool get success {
    _$successAtom.reportRead();
    return super.success;
  }

  @override
  set success(bool value) {
    _$successAtom.reportWrite(value, super.success, () {
      super.success = value;
    });
  }

  late final _$biometricsAvailableAtom =
      Atom(name: '_BiometricStore.biometricsAvailable', context: context);

  @override
  bool get biometricsAvailable {
    _$biometricsAvailableAtom.reportRead();
    return super.biometricsAvailable;
  }

  @override
  set biometricsAvailable(bool value) {
    _$biometricsAvailableAtom.reportWrite(value, super.biometricsAvailable, () {
      super.biometricsAvailable = value;
    });
  }

  late final _$resetAsyncAction =
      AsyncAction('_BiometricStore.reset', context: context);

  @override
  Future<dynamic> reset() {
    return _$resetAsyncAction.run(() => super.reset());
  }

  late final _$checkBiometricsAsyncAction =
      AsyncAction('_BiometricStore.checkBiometrics', context: context);

  @override
  Future<dynamic> checkBiometrics() {
    return _$checkBiometricsAsyncAction.run(() => super.checkBiometrics());
  }

  late final _$authenticateAsyncAction =
      AsyncAction('_BiometricStore.authenticate', context: context);

  @override
  Future<bool> authenticate() {
    return _$authenticateAsyncAction.run(() => super.authenticate());
  }

  @override
  String toString() {
    return '''
loading: ${loading},
success: ${success},
biometricsAvailable: ${biometricsAvailable},
canUseBiometrics: ${canUseBiometrics},
canUseFingerprint: ${canUseFingerprint}
    ''';
  }
}
