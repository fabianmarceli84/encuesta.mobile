// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'setting_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$SettingStore on _SettingStore, Store {
  late final _$loadingAtom =
      Atom(name: '_SettingStore.loading', context: context);

  @override
  bool get loading {
    _$loadingAtom.reportRead();
    return super.loading;
  }

  @override
  set loading(bool value) {
    _$loadingAtom.reportWrite(value, super.loading, () {
      super.loading = value;
    });
  }

  late final _$successAtom =
      Atom(name: '_SettingStore.success', context: context);

  @override
  bool get success {
    _$successAtom.reportRead();
    return super.success;
  }

  @override
  set success(bool value) {
    _$successAtom.reportWrite(value, super.success, () {
      super.success = value;
    });
  }

  late final _$canFingerprintAtom =
      Atom(name: '_SettingStore.canFingerprint', context: context);

  @override
  bool get canFingerprint {
    _$canFingerprintAtom.reportRead();
    return super.canFingerprint;
  }

  @override
  set canFingerprint(bool value) {
    _$canFingerprintAtom.reportWrite(value, super.canFingerprint, () {
      super.canFingerprint = value;
    });
  }

  late final _$automaticLoginAtom =
      Atom(name: '_SettingStore.automaticLogin', context: context);

  @override
  bool get automaticLogin {
    _$automaticLoginAtom.reportRead();
    return super.automaticLogin;
  }

  @override
  set automaticLogin(bool value) {
    _$automaticLoginAtom.reportWrite(value, super.automaticLogin, () {
      super.automaticLogin = value;
    });
  }

  late final _$errorAmountAtom =
      Atom(name: '_SettingStore.errorAmount', context: context);

  @override
  int get errorAmount {
    _$errorAmountAtom.reportRead();
    return super.errorAmount;
  }

  @override
  set errorAmount(int value) {
    _$errorAmountAtom.reportWrite(value, super.errorAmount, () {
      super.errorAmount = value;
    });
  }

  late final _$changeFingerprintAuthAsyncAction =
      AsyncAction('_SettingStore.changeFingerprintAuth', context: context);

  @override
  Future<dynamic> changeFingerprintAuth(bool value) {
    return _$changeFingerprintAuthAsyncAction
        .run(() => super.changeFingerprintAuth(value));
  }

  late final _$changeAutomaticLoginAsyncAction =
      AsyncAction('_SettingStore.changeAutomaticLogin', context: context);

  @override
  Future<dynamic> changeAutomaticLogin(bool value) {
    return _$changeAutomaticLoginAsyncAction
        .run(() => super.changeAutomaticLogin(value));
  }

  late final _$isFraudFingerprintAuthAsyncAction =
      AsyncAction('_SettingStore.isFraudFingerprintAuth', context: context);

  @override
  Future<bool> isFraudFingerprintAuth() {
    return _$isFraudFingerprintAuthAsyncAction
        .run(() => super.isFraudFingerprintAuth());
  }

  late final _$logoutAsyncAction =
      AsyncAction('_SettingStore.logout', context: context);

  @override
  Future<dynamic> logout() {
    return _$logoutAsyncAction.run(() => super.logout());
  }

  late final _$reloadAsyncAction =
      AsyncAction('_SettingStore.reload', context: context);

  @override
  Future<dynamic> reload() {
    return _$reloadAsyncAction.run(() => super.reload());
  }

  @override
  String toString() {
    return '''
loading: ${loading},
success: ${success},
canFingerprint: ${canFingerprint},
automaticLogin: ${automaticLogin},
errorAmount: ${errorAmount}
    ''';
  }
}
