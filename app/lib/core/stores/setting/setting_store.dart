// ignore_for_file: unnecessary_this, non_constant_identifier_names, unused_field, library_private_types_in_public_api

import 'package:app_mobile_encuesta/core/stores/error/error_store.dart';
import 'package:app_mobile_encuesta/data/repository.dart';
import 'package:mobx/mobx.dart';

part 'setting_store.g.dart';

class SettingStore = _SettingStore with _$SettingStore;

abstract class _SettingStore with Store {
  final String TAG = "_SettingStore";

  // repository instance
  final Repository _repository;

  // store for handling error messages
  final ErrorStore errorStore = ErrorStore();

  // constructor:---------------------------------------------------------------
  _SettingStore(Repository repository) : this._repository = repository {
    _setupValidations();
    init();
  }

  // disposers:-----------------------------------------------------------------
  late List<ReactionDisposer> _disposers;

  void _setupValidations() {
    _disposers = [];
  }

  // empty values

  // store variables:-----------------------------------------------------------
  final int limitErrorAmount = 3;

  @observable
  bool loading = false;

  @observable
  bool success = false;

  @observable
  bool canFingerprint = false;

  @observable
  bool automaticLogin = true;

  @observable
  int errorAmount = 0;

  // computed:------------------------------------------------------------------

  // actions:-------------------------------------------------------------------
  @action
  Future changeFingerprintAuth(bool value) async {
    loading = true;

    canFingerprint = value;
    await _repository.changeFingerprintAuth(value);

    loading = false;
  }

  @action
  Future changeAutomaticLogin(bool value) async {
    loading = true;

    automaticLogin = value;
    await _repository.changeAutomaticLogin(value);

    loading = false;
  }

  @action
  Future<bool> isFraudFingerprintAuth() async {
    errorAmount++;

    if (errorAmount >= limitErrorAmount) {
      canFingerprint = false;
      await _repository.changeFingerprintAuth(false);
      return true;
    }

    return false;
  }

  @action
  Future logout() async {
    await _repository.changeFingerprintAuth(false);
    await _repository.changeAutomaticLogin(false);
  }

  @action
  Future reload() async {
    await init();
  }

  // general methods:-----------------------------------------------------------
  Future init() async {
    canFingerprint = _repository.isFingerprintAuth;
    automaticLogin = _repository.isAutomaticLogin;
  }

  // dispose:-------------------------------------------------------------------
  dispose() {}
}
