// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'success_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$SuccessStore on _SuccessStore, Store {
  late final _$messageAtom =
      Atom(name: '_SuccessStore.message', context: context);

  @override
  String get message {
    _$messageAtom.reportRead();
    return super.message;
  }

  @override
  set message(String value) {
    _$messageAtom.reportWrite(value, super.message, () {
      super.message = value;
    });
  }

  late final _$_SuccessStoreActionController =
      ActionController(name: '_SuccessStore', context: context);

  @override
  void setMessage(String message) {
    final _$actionInfo = _$_SuccessStoreActionController.startAction(
        name: '_SuccessStore.setMessage');
    try {
      return super.setMessage(message);
    } finally {
      _$_SuccessStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  void reset(String value) {
    final _$actionInfo = _$_SuccessStoreActionController.startAction(
        name: '_SuccessStore.reset');
    try {
      return super.reset(value);
    } finally {
      _$_SuccessStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic dispose() {
    final _$actionInfo = _$_SuccessStoreActionController.startAction(
        name: '_SuccessStore.dispose');
    try {
      return super.dispose();
    } finally {
      _$_SuccessStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
message: ${message}
    ''';
  }
}
