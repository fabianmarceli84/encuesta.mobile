// ignore_for_file: unnecessary_this, library_private_types_in_public_api

import 'package:mobx/mobx.dart';

part 'success_store.g.dart';

class SuccessStore = _SuccessStore with _$SuccessStore;

abstract class _SuccessStore with Store {
  // constructor:---------------------------------------------------------------
  _SuccessStore() {
    _setupValidations();
  }
  // disposers:-----------------------------------------------------------------
  late List<ReactionDisposer> _disposers;

  void _setupValidations() {
    _disposers = [
      reaction((_) => message, reset, delay: 200),
    ];
  }

  // store variables:-----------------------------------------------------------
  @observable
  String message = '';

  // actions:-------------------------------------------------------------------
  @action
  void setMessage(String message) {
    this.message = message;
  }

  @action
  void reset(String value) {
    message = '';
  }

  // dispose:-------------------------------------------------------------------
  @action
  dispose() {
    for (final d in _disposers) {
      d();
    }
  }
}
