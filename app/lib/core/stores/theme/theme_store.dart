// ignore_for_file: unnecessary_this, non_constant_identifier_names, library_private_types_in_public_api

import 'package:app_mobile_encuesta/core/stores/error/error_store.dart';
import 'package:app_mobile_encuesta/data/repository.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

part 'theme_store.g.dart';

class ThemeStore = _ThemeStore with _$ThemeStore;

abstract class _ThemeStore with Store {
  final String TAG = "_ThemeStore";

  // repository instance
  final Repository _repository;

  // store for handling errors
  final ErrorStore errorStore = ErrorStore();

  // store variables:-----------------------------------------------------------
  @observable
  bool _darkMode = false;

  @observable
  bool _fullScreenMode = false;

  @observable
  int _primaryColor = 0xFFA20101;

  // getters:-------------------------------------------------------------------
  bool get darkMode => _darkMode;

  bool get fullScreenMode => _fullScreenMode;

  int get primaryColor => _primaryColor;

  // constructor:---------------------------------------------------------------
  _ThemeStore(Repository repository) : this._repository = repository {
    init();
  }

  // actions:-------------------------------------------------------------------
  @action
  Future changeBrightnessToDark(bool value) async {
    _darkMode = value;
    await _repository.changeBrightnessToDark(value);
  }

  @action
  Future changeFullScreenMode(bool value) async {
    _fullScreenMode = value;
    await _repository.changeFullScreenMode(value);
  }

  @action
  Future changePrimaryColor(int value) async {
    _primaryColor = value;
    await _repository.changePrimaryColor(value);
  }

  // general methods:-----------------------------------------------------------
  Future init() async {
    _darkMode = _repository.isDarkMode;
    _fullScreenMode = _repository.isFullScreenMode;
    _primaryColor = _repository.primaryColor;
  }

  bool isPlatformDark(BuildContext context) =>
      MediaQuery.platformBrightnessOf(context) == Brightness.dark;

  // dispose:-------------------------------------------------------------------
  dispose() {}
}
