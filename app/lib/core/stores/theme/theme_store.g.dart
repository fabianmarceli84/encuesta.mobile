// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'theme_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$ThemeStore on _ThemeStore, Store {
  late final _$_darkModeAtom =
      Atom(name: '_ThemeStore._darkMode', context: context);

  @override
  bool get _darkMode {
    _$_darkModeAtom.reportRead();
    return super._darkMode;
  }

  @override
  set _darkMode(bool value) {
    _$_darkModeAtom.reportWrite(value, super._darkMode, () {
      super._darkMode = value;
    });
  }

  late final _$_fullScreenModeAtom =
      Atom(name: '_ThemeStore._fullScreenMode', context: context);

  @override
  bool get _fullScreenMode {
    _$_fullScreenModeAtom.reportRead();
    return super._fullScreenMode;
  }

  @override
  set _fullScreenMode(bool value) {
    _$_fullScreenModeAtom.reportWrite(value, super._fullScreenMode, () {
      super._fullScreenMode = value;
    });
  }

  late final _$_primaryColorAtom =
      Atom(name: '_ThemeStore._primaryColor', context: context);

  @override
  int get _primaryColor {
    _$_primaryColorAtom.reportRead();
    return super._primaryColor;
  }

  @override
  set _primaryColor(int value) {
    _$_primaryColorAtom.reportWrite(value, super._primaryColor, () {
      super._primaryColor = value;
    });
  }

  late final _$changeBrightnessToDarkAsyncAction =
      AsyncAction('_ThemeStore.changeBrightnessToDark', context: context);

  @override
  Future<dynamic> changeBrightnessToDark(bool value) {
    return _$changeBrightnessToDarkAsyncAction
        .run(() => super.changeBrightnessToDark(value));
  }

  late final _$changeFullScreenModeAsyncAction =
      AsyncAction('_ThemeStore.changeFullScreenMode', context: context);

  @override
  Future<dynamic> changeFullScreenMode(bool value) {
    return _$changeFullScreenModeAsyncAction
        .run(() => super.changeFullScreenMode(value));
  }

  late final _$changePrimaryColorAsyncAction =
      AsyncAction('_ThemeStore.changePrimaryColor', context: context);

  @override
  Future<dynamic> changePrimaryColor(int value) {
    return _$changePrimaryColorAsyncAction
        .run(() => super.changePrimaryColor(value));
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
