// To parse this JSON data, do
//
//     final session = sessionFromMap(jsonString);

import 'dart:convert';

class Session {
  Session({
    required this.userCode,
    required this.userCodeType,
    required this.userName,
    required this.password,
  });

  final int userCode;
  final int userCodeType;
  final String userName;
  final String password;

  factory Session.fromJson(String str) => Session.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Session.fromMap(Map<String, dynamic> json) => Session(
        userCode: json['userCode'],
        userCodeType: json['userCodeType'],
        userName: json['userName'],
        password: json['password'],
      );

  Map<String, dynamic> toMap() => {
        'userCode': userCode,
        'userCodeType': userCodeType,
        'userName': userName,
        'password': password,
      };
}
