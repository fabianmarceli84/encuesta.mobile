class OptionItem {
  OptionItem({
    required this.id,
    required this.value,
  });

  final int id;
  final String value;
}
