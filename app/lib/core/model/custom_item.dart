import 'package:flutter/material.dart';

class CustomItem {
  CustomItem({
    required this.iconData,
    required this.label,
    required this.nextPage,
  });

  final IconData iconData;
  final String label;
  final Widget nextPage;
}
