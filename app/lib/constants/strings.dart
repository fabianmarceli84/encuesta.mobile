class AppInfo {
  AppInfo._();

  //General
  static const String appName = "Encuesta";

  static const String channelGroupkey = 'basic_channel_group';
  static const String channelKey = 'basic_channel';
}
