///
/// Creating custom color palettes is part of creating a custom app. The idea is to create
/// your class of custom colors, in this case `CompanyColors` and then create a `ThemeData`
/// object with those colors you just defined.
///
/// Resource:
/// A good resource would be this website: http://mcg.mbitson.com/
/// You simply need to put in the colour you wish to use, and it will generate all shades
/// for you. Your primary colour will be the `500` value.
///
/// Colour Creation:
/// In order to create the custom colours you need to create a `Map<int, Color>` object
/// which will have all the shade values. `const Color(0xFF...)` will be how you create
/// the colours. The six character hex code is what follows. If you wanted the colour
/// #114488 or #D39090 as primary colours in your theme, then you would have
/// `const Color(0x114488)` and `const Color(0xD39090)`, respectively.
///
/// Usage:
/// In order to use this newly created theme or even the colours in it, you would just
/// `import` this file in your project, anywhere you needed it.
/// `import 'path/to/theme.dart';`
///

// ignore_for_file: no_leading_underscores_for_local_identifiers

import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:app_mobile_encuesta/constants/font_family.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppTheme {
  AppTheme._(); // this basically makes it so you can't instantiate this class

  static ThemeData getThemeData(int primaryColor) {
    final _primaryColor = Color(primaryColor);
    return ThemeData(
      appBarTheme: AppBarTheme(
        systemOverlayStyle: SystemUiOverlayStyle.light,
        foregroundColor: _primaryColor,
      ),
      fontFamily: FontFamily.productSans,
      primaryColor: _primaryColor,
      brightness: Brightness.light,
      textSelectionTheme: TextSelectionThemeData(
        cursorColor: _primaryColor,
        selectionColor: AppColor.selectionColor,
        selectionHandleColor: _primaryColor,
      ),
      inputDecorationTheme: const InputDecorationTheme(
        labelStyle: TextStyle(
          color: Colors.black,
          fontSize: 14,
        ),
        hintStyle: TextStyle(color: Colors.grey),
      ),
      colorScheme: ColorScheme.fromSwatch(
        primarySwatch: MaterialColor(primaryColor, AppColor.primaries),
        brightness: Brightness.light,
      ).copyWith(
        primary: _primaryColor,
        secondary: _primaryColor,
        background: AppColor.backgroundItemLight,
      ),
      textTheme: ThemeData.light().textTheme.copyWith(
            button: ThemeData.light()
                .textTheme
                .button
                ?.copyWith(color: Colors.white),
          ),
      iconTheme: const IconThemeData(color: Colors.grey),
      checkboxTheme: ThemeData.light().checkboxTheme.copyWith(
            checkColor: MaterialStateProperty.all(Colors.white),
            fillColor: MaterialStateProperty.resolveWith(
              (states) {
                Color fillColor = Colors.grey;
                if (states.contains(MaterialState.selected)) {
                  fillColor = _primaryColor;
                }
                return fillColor;
              },
            ),
          ),
    );
  }

  static ThemeData getThemeDataDark(int primaryColor) {
    final _primaryColor = Color(primaryColor);
    return ThemeData(
      appBarTheme: const AppBarTheme(
        systemOverlayStyle: SystemUiOverlayStyle.light,
      ),
      fontFamily: FontFamily.productSans,
      primaryColor: _primaryColor,
      brightness: Brightness.dark,
      textSelectionTheme: TextSelectionThemeData(
        cursorColor: _primaryColor,
        selectionColor: AppColor.selectionColor,
        selectionHandleColor: _primaryColor,
      ),
      inputDecorationTheme: const InputDecorationTheme(
        labelStyle: TextStyle(
          color: Colors.white,
          fontSize: 14,
        ),
        hintStyle: TextStyle(color: Colors.grey),
      ),
      colorScheme: ColorScheme.fromSwatch(
        primarySwatch: MaterialColor(primaryColor, AppColor.primaries),
        brightness: Brightness.dark,
      ).copyWith(
        primary: _primaryColor,
        secondary: _primaryColor,
        background: AppColor.backgroundItemDark,
      ),
      textTheme: ThemeData.dark().textTheme.copyWith(
            button: ThemeData.dark()
                .textTheme
                .button
                ?.copyWith(color: Colors.white),
          ),
      iconTheme: IconThemeData(color: _primaryColor),
      checkboxTheme: ThemeData.light().checkboxTheme.copyWith(
            checkColor: MaterialStateProperty.all(Colors.black),
            fillColor: MaterialStateProperty.resolveWith(
              (states) {
                return _primaryColor;
              },
            ),
          ),
    );
  }
}
