import 'package:flutter/material.dart';

class AppColor {
  AppColor._(); // this basically makes it so you can't instantiate this class

  static List<ColorSwatch> materialColors = <ColorSwatch>[
    const ColorSwatch(0xFFA20101, {500: Color(0xFFA20101)}),
    const ColorSwatch(0xFF303130, {500: Color(0xFF303130)}),
    Colors.red,
    Colors.pink,
    Colors.purple,
    Colors.deepPurple,
    Colors.indigo,
    Colors.blue,
    Colors.lightBlue,
    Colors.cyan,
    Colors.teal,
    Colors.green,
    Colors.lightGreen,
    Colors.lime,
    // Colors.yellow,
    // Colors.amber,
    Colors.orange,
    Colors.deepOrange,
    Colors.brown,
    Colors.grey,
    Colors.blueGrey,
  ];

  static Map<int, Color> orange = <int, Color>{
    50: const Color(0xFFFCF2E7),
    100: const Color(0xFFF8DEC3),
    200: const Color(0xFFF3C89C),
    300: const Color(0xFFEEB274),
    400: const Color(0xFFEAA256),
    500: const Color(0xFFE69138),
    600: const Color(0xFFE38932),
    700: const Color(0xFFDF7E2B),
    800: const Color(0xFFDB7424),
    900: const Color(0xFFD56217)
  };

  static Map<int, Color> primaries = <int, Color>{
    50: const Color(0xFFFFEBEE),
    100: const Color(0xFFFFCDD2),
    200: const Color(0xFFEF9A9A),
    300: const Color(0xFFE57373),
    400: const Color(0xFFEF5350),
    500: const Color(0xFFF44336),
    600: const Color(0xFFE53935),
    700: const Color(0xFFD32F2F),
    800: const Color(0xFFB71C1C),
    900: primaryColor
  };

  static Color primaryColor =
      const Color(0xFFA20101); //Color.fromARGB(255, 1, 30, 162)
  // static const Color primaryColor = Color.fromARGB(255, 100, 7, 7);
  static const Color accent = Colors.redAccent;
  static const Color selectionColor = Color(0x1EA1A1A1);
  static const Color grayLight = Color(0xFFF1F1F1);
  static const Color grayDark = Color(0x10F6F6F6);
  static const Color grayChatLight = Color(0xFFF1F1F1);
  static const Color grayChatDark = Color(0x9FF9F9F9);
  static const Color grayShimmerLight = Color(0xFFEEEEEE);
  static const Color grayShimmerDark = Color(0x10F6F6F6);

  static const Color backgroundLight = Color(0xFFF7F9FA);
  static const Color backgroundDark = Colors.black;

  static const Color backgroundItemLight = Color.fromARGB(255, 238, 238, 238);
  static const Color backgroundItemDark = Color.fromARGB(255, 19, 19, 19);

  static const Color backgroundTextFormField =
      Color.fromARGB(30, 161, 161, 161);
}
