import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Message {
  Message._();

  static showErrorMessage(
    BuildContext context,
    String message,
  ) {
    if (message.isNotEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        if (message.isNotEmpty) {
          Flushbar(
            title: 'Error',
            message: message,
            icon: const Icon(
              Icons.error_outline_rounded,
              color: Colors.red,
            ),
            margin: const EdgeInsets.all(8),
            borderRadius: BorderRadius.circular(8),
            duration: const Duration(seconds: 3),
          ).show(context);
        }
      });
    }

    return const SizedBox.shrink();
  }

  static showMessage(
    BuildContext context,
    String message,
    bool success,
  ) {
    if (message.isNotEmpty) {
      Future.delayed(const Duration(milliseconds: 0), () {
        if (message.isNotEmpty) {
          Flushbar(
            title: success ? 'Éxito' : 'Error',
            message: message,
            icon: Icon(
              success
                  ? Icons.check_circle_outline_rounded
                  : Icons.error_outline_rounded,
              color: success ? Colors.green : Colors.red,
            ),
            margin: const EdgeInsets.all(8),
            borderRadius: BorderRadius.circular(8),
            duration: const Duration(seconds: 3),
          ).show(context);
        }
      });
    }

    return const SizedBox.shrink();
  }

  static void showToast({
    required String message,
    Color? color,
    Direction? direction,
    bool toastLengthLong = false,
  }) {
    Fluttertoast.cancel();
    Fluttertoast.showToast(
      msg: message,
      toastLength: toastLengthLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT,
      gravity: directionValues.map[direction],
      timeInSecForIosWeb: 30,
      backgroundColor: color ?? AppColor.primaryColor,
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

  static void hideToast() {
    Fluttertoast.cancel();
  }
}

enum PositionToast {
  TOP,
  BOTTOM,
}

final _PositionToastValues positionValues = _PositionToastValues({
  PositionToast.TOP: FlushbarPosition.TOP,
  PositionToast.BOTTOM: FlushbarPosition.BOTTOM,
});

class _PositionToastValues<T> {
  final Map<PositionToast, T> map;

  _PositionToastValues(this.map);
}

enum Direction {
  TOP,
  BOTTOM,
  CENTER,
}

final _DirectionValues directionValues = _DirectionValues({
  Direction.TOP: ToastGravity.TOP,
  Direction.BOTTOM: ToastGravity.BOTTOM,
  Direction.CENTER: ToastGravity.CENTER,
});

class _DirectionValues<T> {
  final Map<Direction, T> map;

  _DirectionValues(this.map);
}
