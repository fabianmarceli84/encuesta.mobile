import 'package:app_mobile_encuesta/utils/security/encryptions.dart';
import 'package:app_mobile_encuesta/utils/utils.dart';

extension StringEncryptions on String {
  String encryptSha1() {
    return Encryptions.toSha1(this);
  }

  String encryptSha256() {
    return Encryptions.toSha256(this);
  }

  String encryptSha512() {
    return Encryptions.toSha512(this);
  }

  String encryptAES() {
    return Encryptions.toAES(this);
  }

  String decryptAES() {
    return Encryptions.fromAES(this);
  }
}

extension DateTimeString on DateTime {
  String toStringFormat({String? format}) {
    return Utils.dateFormat(this, format: format);
  }
}

extension StringExtension on String {
  bool isOverLength(int value) {
    return length > value;
  }

  String replaceOverLength(int value, {String text = '...'}) {
    return length > value ? substring(0, value) + text : this;
  }

  String capitalize() {
    if (length < 1) return this;
    return toLowerCase()
        .split(" ")
        .map((word) => word[0].toUpperCase() + word.substring(1, word.length))
        .join(" ");
  }

  DateTime toDateTime({String? format}) {
    return Utils.stringToDateFormat(this, format: format);
  }

  String toAnotherDateTimeFormat({String? formatInput, String? formatOutput}) {
    return toDateTime(format: formatInput).toStringFormat(format: formatOutput);
  }
}

extension IntExtension on int {
  String toTimeFormat() {
    return Utils.intToTimeLeft(this);
  }
}
