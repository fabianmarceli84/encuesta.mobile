import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:app_mobile_encuesta/core/model/option_item.dart';
import 'package:app_mobile_encuesta/ui/widgets/progress_indicator_widget.dart';
import 'package:app_mobile_encuesta/ui/widgets/text_form_field_custom.dart';
import 'package:app_mobile_encuesta/utils/style/button_style.dart';
import 'package:app_mobile_encuesta/utils/validators/validators.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DialogUtil {
  DialogUtil._();

  static show(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (_) {
        return const CustomProgressIndicatorWidget();
      },
    );
  }

  static hide(BuildContext context) {
    Navigator.of(context).pop();
  }

  static showAlert(
    BuildContext context,
    String text, {
    Widget icon = const Icon(CupertinoIcons.info_circle, color: Colors.grey),
    String title = 'Información',
    String buttonText = 'Aceptar',
    Color? backgroundColor,
    VoidCallback? onPressed,
    bool barrierDismissible = true,
    bool onWillPop = true,
  }) {
    showDialog(
      barrierDismissible: barrierDismissible,
      context: context,
      builder: (_) {
        final size = MediaQuery.of(context).size;
        return WillPopScope(
          onWillPop: () async => onWillPop,
          child: Align(
            alignment: Alignment.center,
            child: FittedBox(
              fit: BoxFit.none,
              child: SizedBox(
                // height: size.height * 0.5,
                width: size.width * 0.8,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4.0),
                  ),
                  color: backgroundColor ??
                      Theme.of(context).colorScheme.background,
                  child: Padding(
                    padding: const EdgeInsets.all(25.0),
                    child: Column(
                      children: [
                        icon,
                        const SizedBox(height: 8.0),
                        Text(
                          title,
                          style: Theme.of(context).textTheme.titleLarge,
                        ),
                        const SizedBox(height: 20.0),
                        Text(
                          text,
                          textAlign: TextAlign.center,
                        ),
                        const SizedBox(height: 20.0),
                        ElevatedButton(
                          style: EncuestaButtonStyle.primary,
                          onPressed:
                              onPressed ?? () => Navigator.of(context).pop(),
                          child: Text(buttonText),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  static showConfirm(
    BuildContext context,
    String text, {
    Widget icon = const Icon(CupertinoIcons.info_circle, color: Colors.grey),
    String title = 'Información',
    String agreeText = 'Aceptar',
    String cancelText = 'Cancelar',
    VoidCallback? agreeCallback,
    VoidCallback? cancelCallback,
    Color? shadowColor,
    Color? backgroundColor,
    bool barrierDismissible = true,
  }) {
    showDialog(
        barrierDismissible: barrierDismissible,
        context: context,
        builder: (_) {
          final size = MediaQuery.of(context).size;
          return Align(
            alignment: Alignment.center,
            child: Container(
              decoration: BoxDecoration(
                color: shadowColor ?? Colors.grey.withAlpha(80),
              ),
              constraints: const BoxConstraints.expand(),
              child: FittedBox(
                fit: BoxFit.none,
                child: SizedBox(
                  // height: size.height * 0.5,
                  width: size.width * 0.8,
                  child: Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                    color: backgroundColor ??
                        Theme.of(context).colorScheme.background,
                    child: Padding(
                      padding: const EdgeInsets.all(25.0),
                      child: Column(
                        children: [
                          icon,
                          const SizedBox(height: 8.0),
                          Text(title,
                              style: Theme.of(context).textTheme.titleLarge),
                          const SizedBox(height: 20.0),
                          Text(text),
                          const SizedBox(height: 20.0),
                          Row(
                            children: [
                              ElevatedButton(
                                style: EncuestaButtonStyle.primary,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                  cancelCallback?.call();
                                },
                                child: Text(cancelText),
                              ),
                              const SizedBox(width: 4.0),
                              ElevatedButton(
                                style: EncuestaButtonStyle.primary,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                  agreeCallback?.call();
                                },
                                child: Text(agreeText),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        });
  }

  static showImputAlert(
    BuildContext context, {
    TextEditingController? textController,
    Widget? icon,
    String? title,
    String? description,
    String labelText = 'Ingrese valor',
    String buttonText = 'Aceptar',
    Color? shadowColor,
    Color? backgroundColor,
    bool barrierDismissible = true,
    int? maxLength,
    int minLines = 1,
    int maxLines = 1,
    ValueChanged? onChanged,
    ValueChanged? onAgree,
  }) {
    showDialog(
      barrierDismissible: barrierDismissible,
      context: context,
      barrierColor: shadowColor ?? Colors.grey.withAlpha(80),
      builder: (_) {
        final size = MediaQuery.of(context).size;
        return Align(
          alignment: Alignment.center,
          child: FittedBox(
            fit: BoxFit.none,
            child: SizedBox(
              // height: size.height * 0.5,
              width: size.width * 0.8,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4.0),
                ),
                color:
                    backgroundColor ?? Theme.of(context).colorScheme.background,
                child: Padding(
                  padding: const EdgeInsets.all(25.0),
                  child: Column(
                    children: [
                      if (icon != null) ...{
                        icon,
                      },
                      if (title != null) ...{
                        const SizedBox(height: 8.0),
                        Text(
                          title,
                          style: Theme.of(context).textTheme.titleLarge,
                        ),
                      },
                      if (description != null) ...{
                        const SizedBox(height: 20.0),
                        Text(description),
                      },
                      const SizedBox(height: 8.0),
                      TextFormFieldCustom(
                        textController: textController,
                        labelText: labelText,
                        textCapitalization: TextCapitalization.sentences,
                        autoFocus: true,
                        regExp: Validators.textCleanPattern,
                        maxLength: maxLength,
                        minLines: minLines,
                        maxLines: maxLines,
                        inputType: TextInputType.multiline,
                        inputAction: TextInputAction.newline,
                        errorText: null,
                        onChanged: onChanged,
                      ),
                      const SizedBox(height: 10.0),
                      ElevatedButton(
                        style: EncuestaButtonStyle.primary,
                        onPressed: () {
                          if (onAgree != null) {
                            onAgree(textController?.text ?? '');
                          }
                          Navigator.of(context).pop();
                        },
                        child: Text(buttonText),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  static showOptions(
    BuildContext context, {
    TextEditingController? textController,
    Widget? icon,
    String title = 'Elige una opción',
    String? description,
    List<OptionItem>? items,
    String cancelText = 'Cancelar',
    ValueChanged<OptionItem>? onSelected,
    Color? shadowColor,
    Color? backgroundColor,
    bool barrierDismissible = true,
  }) {
    showDialog(
      barrierDismissible: barrierDismissible,
      context: context,
      builder: (_) {
        final size = MediaQuery.of(context).size;
        return Align(
          alignment: Alignment.center,
          child: Container(
            decoration: BoxDecoration(
              color: shadowColor ?? Colors.grey.withAlpha(80),
            ),
            constraints: const BoxConstraints.expand(),
            child: FittedBox(
              fit: BoxFit.none,
              child: SizedBox(
                // height: size.height * 0.5,
                width: size.width * 0.8,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4.0),
                  ),
                  color: backgroundColor ??
                      Theme.of(context).colorScheme.background,
                  child: Padding(
                    padding: const EdgeInsets.all(25.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        if (icon != null) ...{
                          icon,
                        },
                        const SizedBox(height: 8.0),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 8.0,
                            vertical: 10.0,
                          ),
                          child: Text(
                            title,
                            style: Theme.of(context).textTheme.titleLarge,
                          ),
                        ),
                        if (description != null) ...{
                          const SizedBox(height: 20.0),
                          Text(description),
                          const SizedBox(height: 8.0),
                        },
                        ...?items?.map(
                          (item) {
                            return InkWell(
                              onTap: () {
                                Navigator.pop(context);
                                if (onSelected != null) {
                                  onSelected(item);
                                }
                              },
                              borderRadius: BorderRadius.circular(4.0),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  item.value.toString(),
                                  textAlign: TextAlign.start,
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyMedium
                                      ?.copyWith(fontSize: 14.0),
                                ),
                              ),
                            );
                          },
                        ).toList(),
                        InkWell(
                          onTap: () => Navigator.pop(context),
                          borderRadius: BorderRadius.circular(4.0),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 10.0),
                            child: Text(
                              cancelText,
                              textAlign: TextAlign.start,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium
                                  ?.copyWith(fontSize: 14.0),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  static showWidgetData(
    BuildContext context,
    Widget widget, {
    bool barrierDismissible = true,
  }) {
    showDialog(
      barrierDismissible: barrierDismissible,
      context: context,
      builder: (_) => Dialog(
        insetPadding: const EdgeInsets.symmetric(
          horizontal: 50,
          vertical: 80,
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 15,
          ),
          child: widget,
        ),
      ),
    );
  }

  static showResponse(
    BuildContext context,
    Widget widget,
    VoidCallback onTapClose, {
    double? horizontal,
    bool barrierDismissible = true,
    bool onWillPop = true,
  }) {
    showDialog(
      barrierDismissible: barrierDismissible,
      context: context,
      builder: (_) => WillPopScope(
        onWillPop: () async => onWillPop,
        child: Dialog(
          insetPadding: EdgeInsets.symmetric(
            horizontal: horizontal ?? 60,
            vertical: 80,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          child: Padding(
            padding: const EdgeInsets.only(
              top: 5,
              right: 5,
              left: 5,
              bottom: 25,
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                GestureDetector(
                  onTap: onTapClose,
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Image.asset(
                      'assets/images/ic_close.png',
                      height: 35,
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                widget,
              ],
            ),
          ),
        ),
      ),
    );
  }

  static showClear(
    BuildContext context,
    String textTitle,
    Widget child,
    VoidCallback onTapClose, {
    double? horizontal,
    bool barrierDismissible = true,
    bool onWillPop = true,
  }) {
    showDialog(
      barrierDismissible: barrierDismissible,
      context: context,
      builder: (_) => WillPopScope(
        onWillPop: () async => onWillPop,
        child: Align(
          child: SingleChildScrollView(
            child: Dialog(
              insetPadding: EdgeInsets.symmetric(
                horizontal: horizontal ?? 60,
                vertical: 80,
              ),
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(20),
                  bottomLeft: Radius.circular(20),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 15,
                  horizontal: 20,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Text(
                            textTitle,
                            style: Theme.of(context)
                                .textTheme
                                .titleMedium
                                ?.copyWith(
                                  color: AppColor.primaryColor,
                                  fontSize: 25.0,
                                  fontWeight: FontWeight.w500,
                                ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        GestureDetector(
                          onTap: onTapClose,
                          child: Align(
                            alignment: Alignment.centerRight,
                            child: Image.asset(
                              'assets/images/ic_close.png',
                              height: 35,
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10.0),
                    Container(
                      color: Colors.grey,
                      height: 2,
                    ),
                    const SizedBox(height: 20.0),
                    child,
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
