// ignore_for_file: overridden_fields, annotate_overrides

import 'package:flutter/material.dart';

class UpperRoute extends PageRouteBuilder {
  final Widget page;
  final Duration transitionDuration;
  final Duration reverseTransitionDuration;
  UpperRoute({
    required this.page,
    this.transitionDuration = const Duration(milliseconds: 300),
    this.reverseTransitionDuration = const Duration(milliseconds: 300),
  }) : super(
          pageBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
          ) =>
              page,
          transitionsBuilder: (
            BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child,
          ) {
            const begin = Offset(0.0, 1.0);
            const end = Offset.zero;
            const curve = Curves.ease;

            var tween =
                Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

            return SlideTransition(
              position: animation.drive(tween),
              child: child,
            );
          },
          transitionDuration: transitionDuration,
          reverseTransitionDuration: reverseTransitionDuration,
        );
}
