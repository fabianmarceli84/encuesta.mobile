import 'package:app_mobile_encuesta/constants/colors.dart';
import 'package:flutter/material.dart';

class EncuestaButtonStyle {
  EncuestaButtonStyle._();

  static final ButtonStyle primary = ElevatedButton.styleFrom(
    textStyle: const TextStyle(fontSize: 20),
    padding: const EdgeInsets.symmetric(
      vertical: 15,
      horizontal: 25,
    ),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(4.0),
    ),
  );

  static Color _getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return Colors.grey;
    }

    return Colors.white;
  }

  static final ButtonStyle iconText = ButtonStyle(
    animationDuration: const Duration(milliseconds: 100),
    shape: MaterialStateProperty.all(
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0))),
    backgroundColor: MaterialStateProperty.all(Colors.transparent),
    overlayColor: MaterialStateProperty.all(Colors.white.withOpacity(0.1)),
    foregroundColor: MaterialStateProperty.resolveWith(_getColor),
  );

  static final ButtonStyle inList = ElevatedButton.styleFrom(
    textStyle: const TextStyle(fontSize: 14),
    padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(4.0),
    ),
  );

  static final ButtonStyle action = ElevatedButton.styleFrom(
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(4.0),
    ),
    backgroundColor: Colors.transparent,
    padding: const EdgeInsets.all(4.0),
    minimumSize: const Size(120, 24),
    maximumSize: const Size(150, 24),
    elevation: 0.0,
    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
    alignment: Alignment.centerLeft,
  );

  static final ButtonStyle leading = ElevatedButton.styleFrom(
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.zero,
    ),
    backgroundColor: AppColor.primaryColor,
    padding: const EdgeInsets.all(4.0),
    minimumSize: const Size(120, 24),
    maximumSize: const Size(150, 24),
    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
    alignment: Alignment.centerLeft,
  );
}
