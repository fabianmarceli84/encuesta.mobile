import 'dart:convert';
import 'package:app_mobile_encuesta/data/network/apis/response.dart';
import 'package:intl/intl.dart';

class Utils {
  Utils._();

  static Map<String, dynamic>? tryJwt(String token) {
    final parts = token.split('.');
    if (parts.length != 3) {
      return null;
    }
    final payload = parts[1];
    var normalized = base64Url.normalize(payload);
    var resp = utf8.decode(base64Url.decode(normalized));
    final payloadMap = json.decode(resp);
    if (payloadMap is! Map<String, dynamic>) {
      return null;
    }
    return payloadMap;
  }

  static String dateFormat(DateTime date, {String? format, String? locale}) {
    DateFormat dateFormat = DateFormat(
      format ?? "dd/MM/yyyy HH:mm:ss",
      locale ?? "es_PE",
    );
    return dateFormat.format(date);
  }

  static String newDateFormat({String? format}) {
    DateTime date = DateTime.now();
    return dateFormat(date, format: format);
  }

  static DateTime stringToDateFormat(String dateString,
      {String? format, String? locale}) {
    DateFormat dateFormat = DateFormat(
      format ?? "dd/MM/yyyy HH:mm:ss",
      locale ?? "es_PE",
    );
    return dateFormat.parse(dateString);
  }

  static String intToTimeLeft(int value) {
    int h, m, s;

    h = value ~/ 3600;

    m = ((value - h * 3600)) ~/ 60;

    s = value - (h * 3600) - (m * 60);

    String hourLeft = h.toString().length < 2 ? "0$h" : h.toString();

    String minuteLeft = m.toString().length < 2 ? "0$m" : m.toString();

    String secondsLeft = s.toString().length < 2 ? "0$s" : s.toString();

    String result = "$hourLeft:$minuteLeft:$secondsLeft";

    return result;
  }

  static String? getErrorMessage(List<ExceptionElement>? exceptions) {
    return (exceptions != null && exceptions.isNotEmpty)
        ? exceptions[0].description
        : null;
  }
}
