import 'package:app_mobile_encuesta/ui/pages/splash/splash.dart';
import 'package:flutter/material.dart';

class Routes {
  Routes._();

  //static variables
  static const String splash = '/splash';
  static const String login = '/login';
  static const String register = '/register';
  static const String registerLocation = '/registerLocation';
  static const String verificationPhoneOne = '/verificationPhoneOne';
  static const String verificationPhoneTwo = '/verificationPhoneTwo';
  static const String lenguage = '/lenguage';
  static const String consumer = '/consumer';
  static const String consumerHome = '/consumerHome';
  static const String consumerSearch = '/consumerSearch';
  static const String consumerFollow = '/consumerFollow';

  static final routes = <String, WidgetBuilder>{
    splash: (BuildContext context) => const SplashScreen(),
    // login: (BuildContext context) => const LoginScreen(),
  };
}
