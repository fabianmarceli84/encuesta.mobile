// ignore_for_file: depend_on_referenced_packages

import 'dart:core';
import 'dart:convert';
import 'package:app_mobile_encuesta/data/constants/data_constants.dart';
import 'package:crypto/crypto.dart';
import 'package:encrypt/encrypt.dart';

class Encryptions {
  static final _aesKey = Key.fromUtf8(SecurityKeyConstants.key);
  static final _aesIv = IV.fromLength(SecurityKeyConstants.iv);

  static String toSha1(String value) {
    var bytes = utf8.encode(value);
    final sha1Result = sha1.convert(bytes);
    return sha1Result.toString();
  }

  static String toSha256(String value) {
    var bytes = utf8.encode(value);
    final sha1Result = sha256.convert(bytes);
    return sha1Result.toString();
  }

  static String toSha512(String value) {
    var bytes = utf8.encode(value);
    final sha1Result = sha512.convert(bytes);
    return sha1Result.toString();
  }

  static String toAES(String value) {
    final encrypter = Encrypter(AES(_aesKey));
    return encrypter.encrypt(value, iv: _aesIv).base64;
  }

  static String fromAES(String value) {
    final encrypter = Encrypter(AES(_aesKey));
    return encrypter.decrypt64(value, iv: _aesIv);
  }
}
