// ignore_for_file: unused_element

import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'package:app_mobile_encuesta/data/constants/data_constants.dart';
import 'package:app_mobile_encuesta/data/local/database/db.dart';
import 'package:drift/drift.dart';
import 'package:drift/isolate.dart';
import 'package:drift/native.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

//Ref: https://drift.simonbinder.eu/docs/advanced-features/isolates/
Future<DriftIsolate> _createDriftIsolate() async {
  final dir = await getApplicationDocumentsDirectory();
  final path = p.join(dir.path, DBConstants.name);
  final receivePort = ReceivePort();

  await Isolate.spawn(
      _startBackground, _IsolateStartRequest(receivePort.sendPort, path),
      debugName: 'database');

  return await receivePort.first as DriftIsolate;
}

void _startBackground(_IsolateStartRequest request) {
  final executor = NativeDatabase(File(request.targetPath));
  final driftIsolate = DriftIsolate.inCurrent(
    () => DatabaseConnection(executor),
  );
  request.sendDriftIsolate.send(driftIsolate);
}

class _IsolateStartRequest {
  final SendPort sendDriftIsolate;
  final String targetPath;

  _IsolateStartRequest(this.sendDriftIsolate, this.targetPath);
}

// This needs to be a top-level method because it's run on a background isolate: only memory
DatabaseConnection _backgroundConnection() {
  final database = NativeDatabase.memory();
  return DatabaseConnection(database);
}

abstract class LocalModule {
  /// A singleton database provider.
  ///
  /// Calling it multiple times will return the same instance.
  static Future<EncuestaDatabase> provideDatabase() async {
    // Start insolate
    DriftIsolate isolate =
        await _createDriftIsolate(); //DriftIsolate.spawn(_backgroundConnection);
    DatabaseConnection connection = await isolate.connect();
    final database = EncuestaDatabase.connect(connection);

    // Return database instance
    return database;
  }

  /// A singleton preference provider.
  ///
  /// Calling it multiple times will return the same instance.
  static Future<SharedPreferences> provideSharedPreferences() {
    return SharedPreferences.getInstance();
  }
}
