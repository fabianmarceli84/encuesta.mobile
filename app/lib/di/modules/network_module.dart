import 'package:app_mobile_encuesta/data/constants/data_constants.dart';
import 'package:dio/dio.dart';

abstract class NetworkModule {
  /// A singleton dio provider.
  /// Calling it multiple times will return the same instance.
  static Dio provideEncuestaServiceDio() {
    final dio = Dio();

    dio
      ..options.baseUrl = Endpoints.encuestaServiceUrl
      ..options.connectTimeout =
          const Duration(milliseconds: Endpoints.connectionTimeout)
      ..options.receiveTimeout =
          const Duration(milliseconds: Endpoints.receiveTimeout)
      ..options.headers = {'Content-Type': 'application/json; charset=utf-8'}
      ..interceptors.add(
        LogInterceptor(
          request: true,
          responseBody: true,
          requestBody: true,
          requestHeader: true,
          // logPrint: (Object object) {
          //   log("$object");
          // },
        ),
      )
      ..interceptors.add(
        InterceptorsWrapper(onRequest:
            (RequestOptions options, RequestInterceptorHandler handler) async {
          return handler.next(options);
        }, onResponse: (Response response, ResponseInterceptorHandler handler) {
          return handler.next(response);
        }),
      );

    return dio;
  }
}
