import 'package:app_mobile_encuesta/core/stores/app/app_store.dart';
import 'package:app_mobile_encuesta/core/stores/biometric/biometric_store.dart';
import 'package:app_mobile_encuesta/core/stores/login/login_store.dart';
import 'package:app_mobile_encuesta/core/stores/setting/setting_store.dart';
import 'package:app_mobile_encuesta/core/stores/theme/theme_store.dart';
import 'package:app_mobile_encuesta/data/local/database/db.dart';
import 'package:app_mobile_encuesta/data/local/spf/shared_preference_helper.dart';
import 'package:app_mobile_encuesta/data/network/apis/encuesta/encuesta_api.dart';
import 'package:app_mobile_encuesta/data/network/dio_client.dart';
import 'package:app_mobile_encuesta/data/repository.dart';
import 'package:app_mobile_encuesta/di/constants/di_constants.dart';
import 'package:app_mobile_encuesta/di/modules/local_module.dart';
import 'package:app_mobile_encuesta/di/modules/network_module.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

final getIt = GetIt.instance;

Future<void> setupLocator() async {
  // // general:-------------------------------------------------------------------

  // // async singletons:----------------------------------------------------------
  getIt.registerSingletonAsync<EncuestaDatabase>(
      () => LocalModule.provideDatabase());
  getIt.registerSingletonAsync<SharedPreferences>(
      () => LocalModule.provideSharedPreferences());

  // // singletons:----------------------------------------------------------------
  getIt.registerSingleton(
      SharedPreferenceHelper(await getIt.getAsync<SharedPreferences>()));
  getIt.registerSingleton<Dio>(
    NetworkModule.provideEncuestaServiceDio(),
    instanceName: NetworkDioInstance.encuesta,
  );

  getIt.registerSingleton(
    DioClient(getIt<Dio>(instanceName: NetworkDioInstance.encuesta)),
    instanceName: NetworkDioClientInstance.encuesta,
  );

  // // api's:---------------------------------------------------------------------
  getIt.registerSingleton(EncuestaApi(
      getIt<DioClient>(instanceName: NetworkDioClientInstance.encuesta)));

  // // repository:----------------------------------------------------------------
  getIt.registerSingleton(Repository(
    await getIt.getAsync<EncuestaDatabase>(),
    getIt<SharedPreferenceHelper>(),
    getIt<EncuestaApi>(),
  ));

  // // stores singleton:----------------------------------------------------------
  /// Solo habrá una instancia de estas clases
  getIt.registerSingleton<ThemeStore>(ThemeStore(getIt<Repository>()));

  // // stores lazy singleton:-----------------------------------------------------
  /// Registra una sola instancia de la clase pero lo registra en tiempo
  /// de ejecución, es decir cuando se llamé en el primer widget
  getIt.registerLazySingleton<AppStore>(() {
    return AppStore(getIt<Repository>());
  });
  getIt.registerLazySingleton<SettingStore>(() {
    return SettingStore(getIt<Repository>());
  });

  // // stores factory-------------------------------------------------------------
  /// Crear una instancia nueva cada vez que se crea
  getIt.registerFactory(() => LoginStore(getIt<Repository>()));
  getIt.registerFactory(() => BiometricStore(getIt<Repository>()));

  // // stores view:---------------------------------------------------------------
  // getIt.registerSingleton(ConsumerViewStore(getIt<Repository>()));
}
